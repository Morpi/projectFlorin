﻿(function () {
    // Private function
    function getColumnsForScaffolding(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ name: propertyName, headerText: propertyName, rowText: propertyName });
        }
        return columns;
    }

    ko.grid = {
        // Defines a view model class you can use to populate a grid
        viewModel: function (configuration) {
            var self = this;

            self.data = configuration.data;
            self.currentPageIndex = ko.observable(0);
            self.pageSize = configuration.pageSize;// || 10000;

            // If you don't specify columns configuration, we'll use scaffolding
            self.columns = configuration.columns || getColumnsForScaffolding(ko.unwrap(self.data));

            self.itemsOnCurrentPage = ko.computed(function () {
                if (!self.pageSize) {
                    return ko.unwrap(self.data);
                }
                var startIndex = self.pageSize * self.currentPageIndex();
                return ko.unwrap(self.data).slice(startIndex, startIndex + self.pageSize);
            });

            self.maxPageIndex = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                return Math.ceil(ko.unwrap(self.data).length / self.pageSize) - 1;
            });
            function _sort(item,sortUp) {
                if (!item.sortable) {
                    return;
                }
                var columnName = item.name;

                configuration.data.sort(function (first, second) {
                    var firstValue = ko.unwrap(first[columnName]);
                    var secondValue = ko.unwrap(second[columnName]);

                    var firstValueFloat = parseFloat(firstValue);
                    var secondValueFloat = parseFloat(secondValue);
                    if (firstValueFloat && secondValueFloat) {
                        if (sortUp) {
                            return firstValueFloat < secondValueFloat ? -1 : 1;
                        }
                        return firstValueFloat > secondValueFloat ? -1 : 1;
                    }

                    if (sortUp) {
                        return firstValue < secondValue ? -1 : 1;
                    }
                    return firstValue > secondValue ? -1 : 1;
                });
            }
            self.sortUp = function (item) {
                _sort(item,true);
            };
            self.sortDown = function (item) {
                _sort(item,false);
            };
           
            self.sortedColumn = ko.observable();

            self.startRange = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                if (self.currentPageIndex() === 0) {
                    return self.currentPageIndex() + 1;
                } else {
                    return self.currentPageIndex() * self.pageSize + 1;
                }
            });
            self.endRange = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                if (self.currentPageIndex() === 0) {
                    if (self.pageSize < ko.unwrap(self.data).length) {
                        return self.pageSize;
                    } else {
                        return ko.unwrap(self.data).length;
                    }
                } else if (self.currentPageIndex() === self.maxPageIndex()) {
                    return ko.unwrap(self.data).length;
                } else {
                    return (self.currentPageIndex() + 1) * self.pageSize;
                }
            });
            self.moveToPrevious = function () {
                if (self.currentPageIndex() === 0) {
                    return false;
                }
                self.currentPageIndex(self.currentPageIndex() - 1);
            };
            self.moveToNext = function () {
                if (self.currentPageIndex() === self.maxPageIndex()) {
                    return false;
                }
                self.currentPageIndex(self.currentPageIndex() + 1);
            };
            self.deleteItem = function (item) {
                configuration.itemDelete(item);
            };
        }
    };

    // Templates used to render the grid
    var templateEngine = new ko.nativeTemplateEngine();

    templateEngine.addTemplate = function (templateName, templateMarkup) {
        document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
    };

    templateEngine.addTemplate("ko_grid_grid", "\
        <div class=\"orders__top\">\
            <div class=\"orders__titles\" data-bind=\"foreach: columns\">\
                <div class=\"orders__title\">\
                    <h3 data-bind=\"html: typeof headerText == 'function' ? headerText($data) : headerText\"></h3>\
                    <div data-bind=\"visible:$data.sortable\" class=\"orders__sort\">\
                            <a href=\"javascript:void(0)\" data-bind=\"click:function(){$parent.sortUp($data);}\">\
                                <i class=\"im im-arrow-up\"></i>\
                            </a>\
                            <a href=\"javascript:void(0)\" data-bind=\"click:function(){$parent.sortDown($data);}\">\
                                <i class=\"im im-arrow-down\"></i>\
                            </a>\
                    </div>\
                </div>\
            </div>\
            <!-- ko foreach: itemsOnCurrentPage -->\
            <div class=\"orders__data\">\
                <!-- ko foreach: $parent.columns -->\
                <div class=\"orders__data--section\">\
                    <!-- ko if: $data.deleteAction -->\
                        <button data-bind=\"click:function(){$root.deleteItem($parent);}\">delete</button>\
                    <!-- /ko -->\
                    <!-- ko if: !$data.deleteAction -->\
                        <span data-bind=\"html: typeof rowText == 'function' ? rowText($parent) : $parent[name]\">\
                        </span>\
                    <!-- /ko -->\
                </div>\
                <!-- /ko -->\
            </div>\
            <!-- /ko -->\
        </div>");

    templateEngine.addTemplate("ko_grid_pageLinks", "\
                    <div class='ko-grid-pageLinks' data-bind='visible:pageSize && ko.unwrap(data).length>0' >\
                        <span>Showing <span data-bind='text: startRange'></span>-<span data-bind='text: endRange'></span> of <span data-bind='text: ko.unwrap(data).length'></span> items</span>\
                        <div class='pull-right'>\
                          <nav>\
                             <ul class='pager' style=\"padding-left: 15px;\">\
                                <li class='active-pager' data-bind='css:{disabled:currentPageIndex()===0},click: moveToPrevious'><a href='javascript:void(0)' aria-hidden='true' aria-label='previous'><img src='/img/arrowleft.gif' /></a></li>\
                                <li class='active-pager' data-bind='css:{disabled:currentPageIndex()=== maxPageIndex()},click: moveToNext'><a href='javascript:void(0)' aria-hidden='true' aria-label='next'><img src='/img/arrowright.gif' /></a></li>\
                             </ul>\
                          </nav>\
                       </div>\
                    </div >");

    // The "grid" binding
    ko.bindingHandlers.grid = {
        init: function () {
            return { 'controlsDescendantBindings': true };
        },
        // This method is called to initialize the node, and will also be called again if you change what the grid is bound to
        update: function (element, viewModelAccessor, allBindings) {
            var viewModel = viewModelAccessor();

            // Empty the element
            while (element.firstChild)
                ko.removeNode(element.firstChild);

            // Allow the default templates to be overridden
            var gridTemplateName = allBindings.get('gridTemplate') || "ko_grid_grid",
                pageLinksTemplateName = allBindings.get('gridPagerTemplate') || "ko_grid_pageLinks";

            // Render the main grid
            var gridContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");

            // Render the page links
            var pageLinksContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(pageLinksTemplateName, viewModel, { templateEngine: templateEngine }, pageLinksContainer, "replaceNode");
        }
    };
})();
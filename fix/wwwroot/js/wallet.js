﻿var DataServices = new function () {
    var unknownError = function () {
        return {
            error: true,
            message: "Unknown error occurred"
        };
    }();

    function getAddress(code) {
        var dfd = jQuery.Deferred();
        Utils.Loading.Show();
        $.get('/Wallet/GetAddress?code=' + code, function (result) {
            Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }

    return {
        GetAddress: getAddress
    };
};

var viewModel = function (data) {
    var root = this;
    root.btcValue = ko.observable();
    root.btcValueDisplay = ko.computed(function () {
        return root.btcValue();
    }).extend({ numeric: 2 });
    root.usdValue = ko.observable();
    root.usdValueDisplay = ko.computed(function () {
        return root.usdValue();
    }).extend({ numeric: 2 });
    root.searchTerm= ko.observable();
    root.showSmallBalances = ko.observable(true);
    root.toggleSmallBalances = function () {
        this.showSmallBalances(!this.showSmallBalances());
    };
    root.allCurrencies = ko.observableArray();

    root.depositModel = new function () {
        var self = this;
        self.code = ko.observable();
        self.name = ko.observable();
        self.address = ko.observable();
        self.minDeposit = ko.observable();
        self.close = function () {
            $('#depositModal').dialog("close");
        };

        self.showModal = function (currency) {
            self.code(currency.code());
            self.name(currency.name());
            self.minDeposit(currency.minDeposit());

            DataServices.GetAddress(currency.code()).then(function (result) {
                if (result.error) {
                    Utils.Notification.Error(result.message);
                }
                else {
                    self.address(result.data);
                    $('#depositModal').modal();
                }
            });
        };
    };

    root.copyText = function () {
        var text = root.depositModel.address();
        Utils.CopyText(text, function () {
            Utils.Notification.Info('Copied');
        });
    };
   
    root.withdrawModel = new function () {
        var self = this;
        self.address = ko.observable();
        self.amount = ko.observable().extend({ numeric: 6 });
        self.available = ko.observable().extend({ numeric: 6 });
        self.fee = ko.observable();
        self.totalAmount = ko.observable();
        self.code = ko.observable();
        self.name = ko.observable();
        self.minWithdrawal = ko.observable();

        self.changeAmount = function () {
            var amount = parseFloat(self.amount());
            if (!amount) {
                return 0;
            }
            self.totalAmount(amount - parseFloat(self.fee()));
        };
        self.availableSelect = function () {
            self.amount(self.available());
            $("#withdraw").validate().element("#withdrawAmount");
            self.changeAmount();
        };
        self.withdraw = function () {
            var self = this;
            var validator = $("#withdraw").validate();
            if (validator.form()) {
                $.post('/Wallet/Withdraw', { address: self.address(), amount: self.amount(), code: self.code() }, function (error) {
                    if (!error) {
                        $('#withdrawModal').modal("hide");
                        $('#withdrawRequestModal').modal("show");
                    }
                    else {
                        Utils.Notification.Error(error);
                    }
                });
            }
        };
        self.withdrawRequest = function () {
            location.reload();
        };
        self.cancel = function () {
            $('#withdrawModal').dialog("close");
        };
        self.showModal = function (currency) {
            Utils.ClearErrors('withdraw');
            self.address('');
            self.amount('');
            self.totalAmount();
            self.code(currency.code());
            self.name(currency.name());
            self.fee(currency.fee());
            self.available(currency.available());
            self.minWithdrawal(currency.minWithdrawal());

            $('#withdrawModal').modal();
        };
    };

    root.currencies = ko.computed(function () {
        return ko.utils.arrayFilter(this.allCurrencies(), function (currency) {
            if (!root.showSmallBalances() && currency.usdValue() < 1) {
                return false;
            }
            var searchTerm = root.searchTerm();
            if (!searchTerm) {
                return true;
            }
            searchTerm = searchTerm.toLowerCase().trim();
            return currency.code().toLowerCase().includes(searchTerm) || currency.name().toLowerCase().includes(searchTerm);
        });
    }, root).extend({ rateLimit: 200 }); 

    function _currency(data) {
        var self = this;

        self.code = ko.observable();
        self.name = ko.observable();
        self.available = ko.observable();
        self.availableDisplay = ko.computed(function () {
            return self.available();
        }).extend({ numeric: 6 });
        self.pendingWithdrawls = ko.observable();
        self.pendingWithdrawlsDisplay = ko.computed(function () {
            return self.pendingWithdrawls();
        }).extend({ numeric: 6 });
        self.onTrades = ko.observable();
        self.onTradesDisplay = ko.computed(function () {
            return self.onTrades();
        }).extend({ numeric: 6 });

        self.total = ko.computed(function () {
            return parseFloat(ko.unwrap(self.available)) + parseFloat(ko.unwrap(self.pendingWithdrawls)) + parseFloat(ko.unwrap(self.onTrades));
        });
        self.totalDisplay = ko.computed(function () {
            return self.total();
        }).extend({ numeric: 6 });
        self.btcValue = ko.observable();
        self.btcValueDisplay = ko.computed(function () {
            return self.btcValue();
        }).extend({ numeric: 6 });
        self.usdValue = ko.observable();
        self.usdValueDisplay = ko.computed(function () {
            return self.usdValue();
        }).extend({ numeric: 2 });
        self.getImage = function (data) {
            return '/img/currencies/{0}.png'.format(data.code());
        };
        self.minWithdrawal = ko.observable();
        self.minDeposit = ko.observable();

        ko.mapping.fromJS(data, {}, self);
    }

    ko.mapping.fromJS(data, {
        'allCurrencies': {
            create: function (options) {
                return new _currency(options.data);
            }
        }
    }, root);
};
var newModel;
function initViewModel(jsonData) {
    var data = JSON.parse(jsonData);
    newModel = new viewModel(data);
    ko.applyBindings(newModel);
}
﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Helpers
{
    public class ExchangeControllerBase : Controller
    {
        protected string UserId
        {
            get
            {
                var claim = base.User.Claims.Single(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
                return claim.Value;
            }
        }
    }
}

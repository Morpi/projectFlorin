﻿var DataServices = new function () {
    var unknownError = function () {
        return {
            error: true,
            message: "Unknown error occurred"
        };
    }();
    function addOrder(data) {
        var dfd = jQuery.Deferred();
        Utils.Loading.Show();
        $.post('/Trade/AddOrder', data, function (result) {
            Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }
    function removeOrder(id) {
        var dfd = jQuery.Deferred();
        Utils.Loading.Show();
        $.post('/Trade/RemoveOrder', { id: id }, function (result) {
            Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }

    return {
        AddOrder: addOrder,
        RemoveOrder: removeOrder
    };
};
var viewModel = function (data, nrOfDecimals, nrOfDecimalsToUSD) {
    var root = this;

    root.toTitle = ko.observable();
    root.from = ko.observable();
    root.to = ko.observable();
    root.priceFromUsd = ko.observable();
    root.orders = ko.observableArray();
    root.menu = ko.observable();
    root.myOrders = ko.observable();
    root.historyOrders = ko.observableArray();
    root.initiateOrders = ko.observableArray();
    root.fee = ko.observable();
    root.marketCode = ko.observable();
    root.balanceFrom = ko.observable();
    root.balanceFromDisplay = ko.computed(function () {
        return root.balanceFrom();
    }).extend({ numeric: nrOfDecimals });
    root.balanceTo = ko.observable();
    root.balanceToDisplay = ko.computed(function () {
        return root.balanceTo();
    }).extend({ numeric: nrOfDecimals });
    root.historySize = ko.observable();

    function notifyFilledOrders(partially, type, from, to, totalFrom, totalTo) {
        totalFrom = Utils.ToFixed(totalFrom, nrOfDecimals);
        totalTo = Utils.ToFixed(totalTo, nrOfDecimals);
        var orderFilledMessage = partially ? 'Partially filled' : 'Fully filled';
        if (type === 'Buy') {
            var messageBuy = '{0} buy order \n {1} {2} for {3} {4}'.format(orderFilledMessage, totalFrom, from, totalTo, to);
            Utils.Notification.Success(messageBuy);
        }
        else {
            var messageSell = '{0} sell order \n {1} {2} for {3} {4}'.format(orderFilledMessage, totalTo, to, totalFrom, from);
            Utils.Notification.Success(messageSell);
        }
    }
    function addFee(input, fee) {
        fee = fee ? fee : root.fee();
        return input * (1 + fee / 100);
    }
    function substractFee(input, fee) {
        fee = fee ? fee : root.fee();
        return input * (1 - fee / 100);
    }
    function getGroupedOrders(sortedOrders) {
        var groupedResults = new Array();
        var prevOrder;
        var sumVolume = 0;
        for (var index in sortedOrders) {
            var sortedOrder = sortedOrders[index];
            sumVolume += parseFloat(sortedOrder.volume());
            if (index == 0 || prevOrder.price() != sortedOrder.price()) {
                prevOrder = new _order({
                    price: sortedOrder.price(),
                    quantity: sortedOrder.quantity(),
                    type: sortedOrder.type(),
                    sumVolume: sumVolume
                });

                groupedResults.push(prevOrder);
            }
            else {
                prevOrder.sumVolume(sumVolume);
                prevOrder.quantity(parseFloat(prevOrder.quantity()) + parseFloat(sortedOrder.quantity()));
            }
        }
        return groupedResults;
    }
    root.buyOrders = ko.computed(function () {
        if (!root.orders) {
            return;
        }
      
        var buyOrders = ko.utils.arrayFilter(root.orders(), function (order) {
            return order.type() === 'Buy';
        });
       
        buyOrders.sort(function (first, second) {
            return second.price() - first.price();
        });
        
        var groupedResults = getGroupedOrders(buyOrders);
        return groupedResults;
    });

    root.selectPrice = function (data) {
        var price = data.price();
        root.initiateBuy.price(price);
        root.initiateSell.price(price);
        root.initiateBuy.changePriceOrQuantity();
        root.initiateSell.changePriceOrQuantity();
        /*
        $("#buyForm").validate().element("#buyTotal");
        $("#buyForm").validate().element("#buyTotal");

        alert('selected2 ' + data.price());
        self.total(root.balanceFrom());
        $("#buyForm").validate().element("#buyTotal");
        self.changeTotal();
        */

    };
    root.sellOrders = ko.computed(function () {
        if (!root.orders) {
            return;
        }

        var sellOrders = ko.utils.arrayFilter(root.orders(), function (order) {
            return order.type() === 'Sell';
        });
        
        sellOrders.sort(function (first, second) {
            return second.price() - first.price();
        });

        var groupedResults = getGroupedOrders(sellOrders.reverse());
        return groupedResults;
    });

    var _menu = function (data) {
        var self = this;
        self.prevLastPrice = ko.observable();
        self.lastPrice = ko.observable();
        self.lastPriceDiff = ko.computed(function () {
            if (!self.prevLastPrice() || !self.lastPrice() || self.prevLastPrice() === self.lastPrice()) {
                return;
            }
            return self.lastPrice() - self.prevLastPrice();
        });
        self.lastPriceCss = ko.computed(function () {
            return self.lastPriceDiff();
        }).extend({ changePriceCss: '' });
        self.lastPriceDisplay = ko.computed(function () {
            return self.lastPrice();
        }).extend({ numeric: nrOfDecimals, displayNA: '' });
        self.lastPriceType = ko.observable();
        self.lastPriceUSD = ko.computed(function () {
            if (!self.lastPrice()) {
                return;
            }
            return self.lastPrice() * root.priceFromUsd();
        });
        self.lastPriceUSDDisplay = ko.computed(function () {
            return self.lastPriceUSD();
        }).extend({ numeric: nrOfDecimalsToUSD });
        self.last24hPrice = ko.observable();
        self._24hChangeValue = ko.computed(function () {
            if (!self.lastPrice() || !self.last24hPrice()) {
                return;
            }
            return self.lastPrice() - self.last24hPrice();
        });
        self._24hChangeCss = ko.computed(function () {
            return self._24hChangeValue();
        }).extend({ changePriceCss: '' });
        self._24hChangeValueDisplay = ko.computed(function () {
            return self._24hChangeValue();
        }).extend({ numeric: nrOfDecimals, displayNA: '', numericSign: '' });
        self._24hChangePercent = ko.computed(function () {
            if (Utils.IsNullOrUndefined(self._24hChangeValue())) {
                return;
            }
            return self._24hChangeValue() / self.lastPrice() * 100;
        });
        self._24hChangePercentDisplay = ko.computed(function () {
            return self._24hChangePercent();
        }).extend({ numeric: 2, numericSign: '' });
        self._24hHigh = ko.observable();
        self._24hHighDisplay = ko.computed(function () {
            return self._24hHigh();
        }).extend({ displayNA: '' });
        self._24hLow = ko.observable();
        self._24hLowDisplay = ko.computed(function () {
            return self._24hLow();
        }).extend({ displayNA: '' });
        self._24hVolume = ko.observable();
        self._24hVolumeDisplay = ko.computed(function () {
            return self._24hVolume();
        }).extend({ displayNA: '' });

        ko.mapping.fromJS(data, {}, self);
    };
    var _order = function (data) {
        var self = this;

        self.id = ko.observable();
        self.type = ko.observable();
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        self.volume = ko.computed(function () {
            return parseFloat(self.price()) * parseFloat(self.quantity()); 
        });
        self.sumVolume = ko.observable().extend({ numeric: nrOfDecimals });

        ko.mapping.fromJS(data, {}, self);
    };
    var _historyOrders = function (data) {
        var self = this;

        self.dateAndTime = ko.observable().extend({ formatOrderTime: '' });
        self.type = ko.observable();
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        ko.mapping.fromJS(data, {}, self);
    };

    function _myPendingOrder(data) {
        var self = this;

        self.id = ko.observable();
        self.dateAndTime = ko.observable();
        self.dateAndTimeDisplay = ko.computed(function () {
            return self.dateAndTime();
        }).extend({ formatOrderTime: '' });
        self.type = ko.observable();
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        self.volume = ko.computed(function () {
            return parseFloat(self.price()) * parseFloat(self.quantity()); //  - parseFloat(self.filled()
        }).extend({ numeric: nrOfDecimals });
        self.filled = ko.observable().extend({ numeric: nrOfDecimals });

        ko.mapping.fromJS(data, {}, self);
    }
    function _myHistoryOrder(data) {
        var self = this;

        self.dateAndTime = ko.observable();
        self.dateAndTimeDisplay = ko.computed(function () {
            return self.dateAndTime();
        }).extend({ formatOrderTime: '' });
        self.type = ko.observable();
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        self.volume = ko.computed(function () {
            return parseFloat(self.price()) * parseFloat(self.quantity());
        }).extend({ numeric: nrOfDecimals });

        ko.mapping.fromJS(data, {}, self);
    }
    var _myOrders = function (data) {
        var self = this;
        self.pendingOrders = ko.observableArray();
        self.historyOrders = ko.observableArray();

        self.removeOrder = function (order) {
            DataServices.RemoveOrder(order.id()).then(function (result) {
                if (result.error) {
                    Utils.Notification.Error(result.message);
                }
            });
        };
        this.myPendingOrdersGrid = new ko.grid.viewModel({
            data: self.pendingOrders,
            columns: [
                {
                    name: 'dateAndTime',
                    headerText: 'Time',
                    rowText: function (item) {
                        return '<span>{0}</span> <span">{1}</span>'.format(item.dateAndTimeDisplay().time, item.dateAndTimeDisplay().date);
                    },
                    sortable: true
                },
                {
                    name: 'type',
                    headerText: "Type",
                    rowText: function (item) {
                        var cssClass = item.type() === 'Sell' ? 'color-red' : 'color-green';
                        return '<span class="{0}">{1}</span>'.format(cssClass, item.type());
                    },
                    sortable: true
                },
                {
                    name: 'price',
                    headerText: "Price",
                    sortable: true
                },
                {
                    name: 'quantity',
                    headerText: function () {
                        return 'Quantity {0}'.format(root.to());
                    },
                    sortable: true
                },
                {
                    name: 'volume',
                    headerText: function () {
                        return 'Volume {0}'.format(root.from());
                    },
                    sortable: true
                },
                {
                    name: 'filled',
                    headerText: function () {
                        return 'Filled {0}'.format(root.to());
                    },
                    sortable: true
                },
                {
                    headerText: '',
                    deleteAction: true
                }
            ],
            itemDelete: function (order) {
                self.removeOrder(order);
            },
            pageSize:4
        });
        self.myHistoryOrdersGrid = new ko.grid.viewModel({
            data: self.historyOrders,
            columns: [{
                name: 'dateAndTime',
                headerText: 'Time',
                rowText: function (item) {
                    return '<span>{0}</span> <span">{1}</span>'.format(item.dateAndTimeDisplay().time, item.dateAndTimeDisplay().date);
                },
                sortable: true
            },
                {
                    name: 'type',
                    headerText: "Type",
                    rowText: function (item) {
                        var cssClass = item.type() === 'Sell' ? 'color-red' : 'color-green';
                        return '<span class="{0}">{1}</span>'.format(cssClass, item.type());
                    },
                    sortable: true
                },
                {
                    name: 'price',
                    headerText: "Price",
                    sortable: true
                },
            {
                name: 'quantity',
                headerText: function () {
                    return 'Quantity {0}'.format(root.to());
                },
                sortable: true
            },
            {
                name: 'volume',
                headerText: function () {
                    return 'Volume {0}'.format(root.from());
                },
                sortable: true
                }],
            pageSize: 4
        });
        ko.mapping.fromJS(data, {
            'pendingOrders': {
                create: function (options) {
                    return new _myPendingOrder(options.data);
                }
            },
            'historyOrders': {
                create: function (options) {
                    return new _myHistoryOrder(options.data);
                }
            }
        }, self);
    };

    var _initiateBuy = function () {
        var self = this;
        self.fee = ko.observable().extend({ numeric: nrOfDecimals });
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        self.total = ko.observable().extend({ numeric: nrOfDecimals });
        self.disable = ko.observable(false);
        self.buy = function () {
            if (self.disable()) {
                return;
            }
            var validator = $("#buyForm").validate();
            if (validator.form()) {
                self.disable(true);
                var data = {
                    marketCode: root.marketCode(),
                    type: 'Buy',
                    price: self.price(),
                    quantity: self.quantity()
                };
                DataServices.AddOrder(data).then(function (result) {
                    if (result.error) {
                        Utils.Notification.Error(result.message);
                    }
                    else {
                        self.price('');
                        self.quantity('');
                        self.total('');
                        self.fee('');
                    }
                    self.disable(false);
                });
            }
        };
        self.selectBalance = function () {
            self.total(root.balanceFrom());
            $("#buyForm").validate().element("#buyTotal");
            self.changeTotal();
        };
        self.changePriceOrQuantity = function () {
            var price = parseFloat(self.price());
            var quantity = parseFloat(self.quantity());
            if (isNaN(price) || isNaN(quantity)) {
                return;
            }
            var fee = price * quantity * root.fee() / 100;
            self.fee(fee);
            var newTotal = price * quantity + fee;
            self.total(newTotal);
            $("#buyForm").validate().element("#buyTotal");
        };
        self.changePrice = function () {
            self.changePriceOrQuantity();
        };
        self.changeQuantity = function () {
            self.changePriceOrQuantity();
        };
        self.changeTotal = function () {
            var total = parseFloat(self.total());
            if (isNaN(total)) {
                return;
            }
            var fee = self.total() * root.fee() / 100;
            self.fee(fee);
            var price = parseFloat(self.price());
            if (!isNaN(price)) {
                var newQuantity = (total - fee) / price;
                self.quantity(newQuantity);
                $("#buyForm").validate().element("#buyQuantity");
            }
        };
    };
    var _initiateSell = function () {
        var self = this;
        self.fee = ko.observable().extend({ numeric: nrOfDecimals });
        self.price = ko.observable().extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable().extend({ numeric: nrOfDecimals });
        self.total = ko.observable().extend({ numeric: nrOfDecimals });
        self.disable = ko.observable(false);
        self.sell = function () {
            if (self.disable()) {
                return;
            }
            var validator = $("#sellForm").validate();
            if (validator.form()) {
                self.disable(true);
                var data = {
                    marketCode: root.marketCode(),
                    type: 'Sell',
                    price: self.price(),
                    quantity: self.quantity()
                };
                DataServices.AddOrder(data).then(function (result) {
                    if (result.error) {
                        Utils.Notification.Error(result.message);
                    }
                    else {
                        self.price('');
                        self.quantity('');
                        self.total('');
                        self.fee('');
                    }
                    self.disable(false);
                });
            }
        };
        self.selectBalance = function () {
            self.quantity(root.balanceTo());
            $("#sellForm").validate().element("#sellQuantity");
            changePriceOrQuantity();
        };
        self.changePriceOrQuantity = function () {
            var price = parseFloat(self.price());
            var quantity = parseFloat(self.quantity());
            if (isNaN(price) || isNaN(quantity)) {
                return;
            }
            var fee = price * quantity * root.fee() / 100;
            self.fee(fee);
            var newTotal = price * quantity - fee;
            self.total(newTotal);
            $("#sellForm").validate().element("#sellTotal");
        };
        self.changePrice = function () {
            self.changePriceOrQuantity();
        };
        self.changeQuantity = function () {
            self.changePriceOrQuantity();
        };
        self.changeTotal = function () {
            var total = parseFloat(self.total());
            if (isNaN(total)) {
                return;
            }
            var fee = self.total() * root.fee() / 100;
            self.fee(fee);
            var price = parseFloat(self.price());
            if (!isNaN(price)) {
                var newQuantity = (total + fee) / price;
                self.quantity(newQuantity);
                $("#sellForm").validate().element("#sellQuantity");
            }
        };
    };
    root.initiateBuy = new _initiateBuy();
    root.initiateSell = new _initiateSell();


    root.setPageTitle = function () {
        var title = '{0}/{1} | Buy or sell {2} | Swapper.Tech'.format(root.from(), root.to(), root.toTitle());
        if (root.menu().lastPrice()) {
            title = root.menu().lastPriceDisplay() + ' | ' + title;
        }
        document.title = title;
    };


    ko.mapping.fromJS(data, {
        'menu': {
            create: function (options) {
                return new _menu(options.data);
            }
        }
    }, root);

    root.setPageTitle();
    
    root.ConnectMarketSocket = function () {
        var settings = {
            marketCode: root.marketCode(),
            listeners:{
                onInit: function (result) {
                    if (!result.error) {
                        var pendingOrdersObs = ko.mapping.fromJS(result.data.pendingOrders, {
                            create: function (options) {
                                return new _order(options.data);
                            }
                        });
                        root.orders(pendingOrdersObs());

                        var historyOrdersObs = ko.mapping.fromJS(result.data.historyOrders, {
                            create: function (options) {
                                return new _historyOrders(options.data);
                            }
                        });
                        root.historyOrders(historyOrdersObs());

                        if (result.data.myOrders) {
                            var myOrderObs = ko.mapping.fromJS(
                                result.data.myOrders,
                                {
                                    create: function (options) {
                                        return new _myOrders(options.data);
                                    }
                                }
                            );
                            root.myOrders(myOrderObs);
                            root.balanceFrom(result.data.balanceFrom);
                            root.balanceTo(result.data.balanceTo);
                        }
                    }
                },
                onRemoveOrder: function (id) {
                    var order = ko.utils.arrayFirst(root.orders(), function (order) {
                        return order.id() === id;
                    });
                    if (order) {
                        root.orders.remove(order);
                    }
                },
                onUserRemoveOrder: function (result) {
                    var market = result.market;
                    var order = result.order;
                    if (root.marketCode() === market.code) {
                        var pendingOrdersObjs = root.myOrders().pendingOrders;
                        var orderFound = ko.utils.arrayFirst(pendingOrdersObjs(), function (item) {
                            return item.id() === order.id;
                        });
                        if (orderFound) {
                            pendingOrdersObjs.remove(orderFound);
                        }
                    }
                    if (order.type === 'Buy' && (market.from === root.from() || market.from === root.to())) {
                        var totalSpent = addFee(parseFloat(order.price) * parseFloat(order.quantity), parseFloat(order.fee));
                        if (market.from === market.from) {
                            root.balanceFrom(parseFloat(root.balanceFrom()) + totalSpent);
                        }
                        else {
                            root.balanceTo(parseFloat(root.balanceTo()) + totalSpent);
                        }
                    }
                    else if (order.type === 'Sell' && (market.to === root.from() || market.to === root.to())) {
                        if (market.to === root.from()) {
                            root.balanceFrom(parseFloat(root.balanceFrom()) + parseFloat(order.quantity));
                        }
                        else {
                            root.balanceTo(parseFloat(root.balanceTo()) + parseFloat(order.quantity));
                        }
                    }
                },
                onAddMarketOrder: function (result) {
                    var newOrder = result.newOrder;
                    if (newOrder) {
                        newOrder.type = result.type;
                        var newOrderObs = ko.mapping.fromJS(newOrder, {
                            create: function (options) {
                                return new _order(options.data);
                            }
                        });
                        root.orders.push(newOrderObs);
                    }

                    var updatedOrder = result.updatedOrder;
                    if (updatedOrder) {
                        ko.utils.arrayForEach(root.orders(), function (order) {
                            if (updatedOrder.id === order.id()) {
                                order.quantity(updatedOrder.newQuantity);
                            }
                        });
                    }

                    if (result.completedOrderIds.length > 0) {
                        ko.utils.arrayForEach(result.completedOrderIds, function (completedOrderId) {
                            var order = ko.utils.arrayFirst(root.orders(), function (order) {
                                return completedOrderId === order.id();
                            });
                            if (order) {
                                root.orders.remove(order);
                            }
                        });
                    }

                    var historyOrders = result.historyOrders;
                    if (historyOrders.length > 0) {
                        ko.utils.arrayForEach(historyOrders, function (order) {
                            order.type = result.type;
                        });
                        var historyOrdersObs = ko.mapping.fromJS(historyOrders, {
                            create: function (options) {
                                return new _historyOrders(options.data);
                            }
                        });
                        ko.utils.arrayForEach(historyOrdersObs(), function (order) {
                            root.historyOrders.unshift(order);
                        });

                        var menu = root.menu();
                        var lastHistoryOrder = historyOrders[historyOrders.length - 1];
                        menu.prevLastPrice(menu.lastPrice());
                        menu.lastPrice(lastHistoryOrder.price);

                        var index = 0;
                        var historySize = parseInt(root.historySize());
                        root.historyOrders.remove(function () {
                            index++;
                            return index > historySize;
                        });

                        if (updateTradeView) {
                            updateTradeView(lastHistoryOrder);
                        }
                    }
                },
                onAddCurrentUserOrder: function (result) {
                    var newOrder = result.newOrder;
                    var market = result.market;
                    if (newOrder) {
                        if (market.code === root.marketCode()) {
                            newOrder.type = result.type;
                            var newOrderObj = ko.mapping.fromJS(newOrder, {
                                create: function (options) {
                                    return new _myPendingOrder(options.data);
                                }
                            });
                            root.myOrders().pendingOrders.unshift(newOrderObj);
                        }

                        // update balance
                        if (result.type === 'Buy' && (market.from === root.from() || market.from === root.to())) {
                            var remainingFrom = parseFloat(newOrder.quantity) - parseFloat(newOrder.filled);
                            var totalSpent = addFee(parseFloat(newOrder.price) * remainingFrom);
                            if (market.from === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) - totalSpent);
                            }
                            else {
                                root.balanceTo(parseFloat(root.balanceTo()) - totalSpent);
                            }
                        }
                        else if (result.type === 'Sell' && (market.to === root.from() || market.to === root.to())) {
                            var remainingTo = parseFloat(newOrder.quantity) - parseFloat(newOrder.filled);
                            if (market.to === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) - remainingTo);
                            }
                            else {
                                root.balanceTo(parseFloat(root.balanceTo()) - remainingTo);
                            }
                        }
                    }

                    var historyOrders = result.historyOrders;
                    if (historyOrders.length > 0) {
                        ko.utils.arrayForEach(historyOrders, function (order) {
                            order.type = result.type;
                        });
                        var historyOrdersObs = ko.mapping.fromJS(historyOrders, {
                            create: function (options) {
                                return new _myHistoryOrder(options.data);
                            }
                        });
                        if (market.code === root.marketCode()) {
                            ko.utils.arrayForEach(historyOrdersObs(), function (order) {
                                root.myOrders().historyOrders.unshift(order);
                            });
                        }
                        var totalFrom = 0;
                        var totalTo = 0;
                        ko.utils.arrayForEach(historyOrdersObs(), function (order) {
                            var price = parseFloat(order.price());
                            var quantity = parseFloat(order.quantity());
                            if (result.type === "Buy") {
                                totalFrom += addFee(price * quantity);
                            }
                            else {
                                totalFrom += substractFee(price * quantity);
                            }
                            totalTo += quantity;
                        });
                        // update balance

                        if (result.type === "Buy") {
                            if (market.from === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) - totalFrom);
                            }
                            else if (market.from === root.to()) {
                                root.balanceTo(parseFloat(root.balanceTo()) - totalFrom);
                            }
                            if (market.to === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) + totalTo);
                            }
                            else if (market.to === root.to()) {
                                root.balanceTo(parseFloat(root.balanceTo()) + totalTo);
                            }
                        }
                        else {
                            if (market.from === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) + totalFrom);
                            }
                            else if (market.from === root.to()) {
                                root.balanceTo(parseFloat(root.balanceTo()) + totalFrom);
                            }
                            if (market.to === root.from()) {
                                root.balanceFrom(parseFloat(root.balanceFrom()) - totalTo);
                            }
                            else if (market.to === root.to()) {
                                root.balanceTo(parseFloat(root.balanceTo()) - totalTo);
                            }
                        }
                        var partially = result.newOrder !== null && result.newOrder !== undefined;
                        notifyFilledOrders(partially, result.type, market.from, market.to, totalFrom, totalTo);
                    }
                    else {
                        Utils.Notification.Success('Order placed successfully');
                    }
                },
                onAddOtherUserOrder: function (result) {
                    var historyOrder = result.historyOrder;

                    var market = result.market;
                    if (market.code === root.marketCode()) {
                        historyOrder.type = result.type;
                        var historyOrdersObs = ko.mapping.fromJS(historyOrder, {
                            create: function (options) {
                                return new _myHistoryOrder(options.data);
                            }
                        });
                        root.myOrders().historyOrders.unshift(historyOrdersObs);

                        if (result.fullyFilled) {
                            var order = ko.utils.arrayFirst(root.myOrders().pendingOrders(), function (order) {
                                return historyOrder.id === order.id();
                            });
                            if (order) {
                                root.myOrders().pendingOrders.remove(order);
                            }
                        }
                        else {
                            ko.utils.arrayForEach(root.myOrders().pendingOrders(), function (order) {
                                if (historyOrder.id === order.id()) {
                                    var filled = parseFloat(historyOrder.quantity);
                                    order.filled(parseFloat(order.filled()) + filled);
                                }
                            });
                        }
                    }

                    // update balance !!
                    var price = parseFloat(historyOrder.price); 
                    var quantity = parseFloat(historyOrder.quantity);
                    var totalFrom = 0;
                    var totalTo = 0;
                    if (result.type === "Buy") {
                        totalTo = quantity;
                        if (market.to === root.from()) {
                            root.balanceFrom(parseFloat(root.balanceFrom()) + totalTo);
                        }
                        else if (market.to === root.to()) {
                            root.balanceTo(parseFloat(root.balanceTo()) + totalTo);
                        }
                    }
                    else {
                        totalFrom = substractFee(price * quantity);
                        if (market.from === root.from()) {
                            root.balanceFrom(parseFloat(root.balanceFrom()) + totalFrom);
                        }
                        else if (market.from === root.to()) {
                            root.balanceTo(parseFloat(root.balanceTo()) + totalFrom);
                        }
                    }
                    if (result.fullyFilled) {
                        totalFrom = result.totalFromWithFees;
                        totalTo = result.totalTo;
                    }
                    notifyFilledOrders(!result.fullyFilled, result.type, market.from, market.to, totalFrom, totalTo);
                },
                onMenuChange: function (result) {
                    var menu = root.menu();
                    menu.last24hPrice(result.last24hPrice);
                    menu._24hHigh(result._24hHigh);
                    menu._24hLow(result._24hLow);
                    menu._24hVolume(result._24hVolume);
                },
                onChangeUSDPrice: function (result) {
                    if (root.from() === result.currencyCode) {
                        root.priceFromUsd(result.newPrice);
                    }
                }
            }
        };
        MarketOrderSocket.Connect(settings);
    };
};
var mainViewModel;
function initViewModel(data, nrOfDecimals, nrOfDecimalsToUSD) {

    var model = new viewModel(data, nrOfDecimals, nrOfDecimalsToUSD);
    mainViewModel = model;
    ko.applyBindings(model);
    model.ConnectMarketSocket();
    //data-val-range="Value must be greater than equal to 0.001"
    $('#buyTotal').attr('data-val-range', 'The value must be between {0} and {1}'.format(model.minTotal(), model.maxTotal()));
    $('#buyTotal').attr('data-val-range-min', model.minTotal());
    $('#buyTotal').attr('data-val-range-max', model.maxTotal());
        
    $('#sellTotal').attr('data-val-range', 'The value must be between {0} and {1}'.format(model.minTotal(), model.maxTotal()));
    $('#sellTotal').attr('data-val-range-min', model.minTotal());
    $('#sellTotal').attr('data-val-range-max', model.maxTotal());
}
$(document).ready(function () {
    $('.orders__tab').each(function () {
        $(this).on('click', function () {
            $('.orders__tab').removeClass('active');
            $(this).addClass('active');

            let cb = $(this).data('target');
            $('.orders__container').removeClass('show');
            $('#' + cb).addClass('show');
        });
    })

    let orders = $('.orders').height();
    $('.orders').css('bottom', '-' + (orders + 6) + 'px');
});

$(window).on('resize', function () {
    let orders = $('.orders').height();
    $('.orders').css('bottom', '-' + (orders + 6) + 'px');
});
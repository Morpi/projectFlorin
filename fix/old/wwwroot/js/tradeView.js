﻿var TradeViewDataServices = new function () {
    function getData(marketCode, from, to, resolution) {
        var dfd = jQuery.Deferred();
        //        Utils.Loading.Show();
        $.get('/Trade/GetTradingViewData?marketCode={0}&from={1}&to={2}&resolution={3}'.format(marketCode, from, to, resolution), function (result) {
            //          Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }

    return {
        GetData: getData
    };
};
var liveTradingInfoListener;
var liveTradingInfolastBar;

var liveTradingInfoResolution;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
const supportedResolutions = ["1", "3", "5", "15", "30", "60", "120", "240", "D"];

const config = {
    supported_resolutions: supportedResolutions
};
var customDataFeed = {
    onReady: function (cb) {
        setTimeout(function () { cb(config); }, 0);
    },
    resolveSymbol: function (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
        var split_data = symbolName.split(/[:/]/);
        var symbol_stub = {
            name: symbolName,
            description: '',
            type: 'crypto',
            session: '24x7',
            timezone: 'Etc/UTC',
            ticker: symbolName,
            exchange: split_data[0],
            minmov: 1,
            pricescale: Math.pow(10, nrOfDecimals),
            has_intraday: true,
            intraday_multipliers: ['1', '60'],
            supported_resolution: supportedResolutions,
            volume_precision: 2,
            data_status: 'streaming'
        };

        setTimeout(function () {
            onSymbolResolvedCallback(symbol_stub);
        }, 0);
    },
    getBars: function (symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) {
        TradeViewDataServices.GetData(mainViewModel.marketCode(), from, to, resolution).then(function (result) {
            if (result.error) {
                alert('error:' + result.message);
            }
            else {
                var bars = result.data;
                if (bars.length) {
                    liveTradingInfoLastBar = bars[bars.length - 1];
                    onHistoryCallback(bars, { noData: false });
                } else {
                    onHistoryCallback(bars, { noData: true });
                }
            }
        });
    },
    calculateHistoryDepth: function (resolution, resolutionBack, intervalBack) {
        // while optional, this makes sure we request 24 hours of minute data at a time
        // CryptoCompare's minute data endpoint will throw an error if we request data beyond 7 days in the past, and return no data
        return resolution < 60 ? { resolutionBack: 'D', intervalBack: '1' } : undefined;
    },
    subscribeBars: function (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
        liveTradingInfoListener = onRealtimeCallback;
        liveTradingInfoResolution = resolution;
    },
    unsubscribeBars: function (subscriberUID) {
    }
};

function initTradingView(from, to, nrOfDecimals) {
    var symbol = '{0}->{1}'.format(from, to);
    var widget = window.tvWidget = new TradingView.widget({
        width: "100%",
        height: "400px",
        // debug: true, // uncomment this line to see Library errors and warnings in the console
        //fullscreen: true,
        symbol: symbol,
        interval: '60',
        container_id: "tv_chart_container",
        //	BEWARE: no trailing slash is expected in feed URL
        datafeed: customDataFeed,
        library_path: "charting_library/",
        locale: getParameterByName('lang') || "en",
        disabled_features: ["header_symbol_search", "timeframes_toolbar", "use_localstorage_for_settings"],
        enabled_features: ["move_logo_to_main_pane", "keep_left_toolbar_visible_on_small_screens"],
        client_id: 'tradingview.com',
        user_id: 'public_user_id',
        theme: getParameterByName('theme'),
        studies_overrides: null
    });
    widget.onChartReady(function () {
        $('#header-toolbar-save-load').parents('[class^="group"]').remove();
    });
}
function updateTradeView(newOrder) {
    //var date = new Date(new Date(newOrder.dateAndTime * 1000));
    //var utc = new Date(date.getTime() + date.getTimezoneOffset() * 60000);

    var tradingOrder = {
        //time: utc.getTime(),
        time: newOrder.dateAndTime * 1000,
        volume: parseFloat(newOrder.quantity),
        price: parseFloat(newOrder.price)
    };
    if (!liveTradingInfoLastBar) {
        liveTradingInfoLastBar = tradingOrder;
        return;
    }

    if (liveTradingInfoLastBar && tradingOrder.time < liveTradingInfoLastBar.time) {
        return;
    }

    var lastBar = updateBar(tradingOrder);

    // send the most recent bar back to TV's realtimeUpdate callback
    liveTradingInfoListener(lastBar);
    // update our own record of lastBar
    liveTradingInfoLastBar = lastBar;
}

// Take a single trade, and subscription record, return updated bar
function updateBar(data) {
    var lastBar = liveTradingInfoLastBar;
    let resolution = liveTradingInfoResolution;
    if (resolution.includes('D')) {
        // 1 day in minutes === 1440
        resolution = 1440;
    } else if (resolution.includes('W')) {
        // 1 week in minutes === 10080
        resolution = 10080;
    }
    var roundedLastTime = data.time / 1000;
    var coeff = resolution * 60;
    var rounded = Math.floor(roundedLastTime / coeff) * coeff;
    var lastBarSec = Math.floor(lastBar.time / 1000);
    var result;

    if (rounded > lastBarSec) {
        // create a new candle, use last close as open **PERSONAL CHOICE**
        result = {
            time: rounded * 1000,
            open: lastBar.close,
            high: lastBar.close,
            low: lastBar.close,
            close: data.price,
            volume: data.volume
        };
    } else {
        // update lastBar candle!
        if (data.price < lastBar.low) {
            lastBar.low = data.price;
        } else if (data.price > lastBar.high) {
            lastBar.high = data.price;
        }

        lastBar.volume += data.volume;
        lastBar.close = data.price;
        lastBar.price = data.price;
        result = lastBar;
    }
    return result;
}
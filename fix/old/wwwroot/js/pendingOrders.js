﻿var DataServices = new function () {
    var unknownError = function () {
        return {
            error: true,
            message: "Unknown error occurred"
        };
    }();
    function removeOrder(id) {
        var dfd = jQuery.Deferred();
        Utils.Loading.Show();
        $.post('/PendingOrders/RemoveOrder?id=' + id, function (result) {
            Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }

    return {
        RemoveOrder: removeOrder
    };
};
var viewModel = function (data, nrOfDecimals) {
    var root = this;
    root.pendingOrders = ko.observableArray();

    function _pendingOrder(data) {
        var self = this;

        self.id = ko.observable();
        self.dateAndTime = ko.observable();
        self.dateAndTimeDisplay = ko.computed(function () {
            return self.dateAndTime();
        }).extend({ formatOrderTime: '' });
        self.from = ko.observable();
        self.marketCode = ko.observable();
        self.to = ko.observable();
        self.type = ko.observable();
        self.price = ko.observable();
        self.priceDisplay = ko.computed(function () {
            return self.price();
        }).extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable();
        self.quantityDisplay = ko.computed(function () {
            return self.quantity();
        }).extend({ numeric: nrOfDecimals });
        self.volume = ko.computed(function () {
            return parseFloat(self.price()) * parseFloat(self.quantity()); 
        }).extend({ numeric: nrOfDecimals });
        self.filled = ko.observable().extend({ numeric: nrOfDecimals });

        self.getImage = function (code) {
            return '/img/currencies/{0}.png'.format(code);
        };
        ko.mapping.fromJS(data, {}, self);
    }
    root.deletePendingOrder = function (data) {
        DataServices.RemoveOrder(data.id()).then(function (result) {
            if (result.error) {
                Utils.Notification.Error(result.message);
            }
            else {
                root.pendingOrders.remove(data);
            }
        });
    };

    ko.mapping.fromJS(data, {
        'pendingOrders': {
            create: function (options) {
                return new _pendingOrder(options.data);
            }
        }
    }, root);
};

var newModel;
function initViewModel(jsonData) {
    if (jsonData) {
        var data = JSON.parse(jsonData);
        newModel = new viewModel(data, data.nrOfDecimals);
    }
    else {
        newModel = new viewModel();
    }
   
    ko.applyBindings(newModel);
}
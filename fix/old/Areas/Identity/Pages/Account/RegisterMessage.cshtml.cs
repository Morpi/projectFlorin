﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebUI.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterMessage : PageModel
    {
        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string ContactEmail { get; set; }

        public void OnGet(string email = null)
        {
            Email = email;
            ContactEmail = Core.ExchangeConsts.CONTACT_EMAIL;
        }
    }
}
﻿var viewModel = function (data, nrOfDecimals) {
    var root = this;
    root.searchTerm = ko.observable();
    root.allMarkets = ko.observableArray();
    root.splittedMarkets = ko.observableArray();
    root.selectedBasedMarket = ko.observable();
    root.getImage = function (data) {
        return '/img/currencies/{0}.png'.format(data);
    };

    root.markets = ko.computed(function () {
        return ko.utils.arrayFilter(root.allMarkets(), function (market) {
            var searchTerm = root.searchTerm();
            if (!searchTerm) {
                return true;
            }
            searchTerm = searchTerm.toLowerCase().trim();
            return market.code().toLowerCase().includes(searchTerm) || market.name().toLowerCase().includes(searchTerm);
        });
    }, root).extend({ rateLimit: 200 });
    root.baseMarkets = ko.computed(function () {
        var result = new Array();
        ko.utils.arrayForEach(root.allMarkets(), function (market) {
            if (result.indexOf(market.from()) === -1) {
                result.push(market.from());
            }
        });
        return result;
    });
    root.changeMarketTo = function (baseMarket) {
        _changeMarketTo(baseMarket);
    };
    
    function _changeMarketTo(baseMarket) {
        if (!baseMarket) {
            baseMarket = root.baseMarkets()[0];
        }
        var result = new Array();
        ko.utils.arrayForEach(root.markets(), function (market) {
            if (market.from() === baseMarket) {
                result.push(market);
            }
        });
        root.splittedMarkets(result);
    }

    function _market(data) {
        var self = this;

        self.from = ko.observable();
        self.to = ko.observable();
        self.currencyName = ko.observable();
        self.code = ko.observable();
        self.lastPrice = ko.observable();
        self.lastPriceDisplay = ko.computed(function () {
            return self.lastPrice();
        }).extend({ numeric: nrOfDecimals, displayNA: '' });
        self.lastPriceUsd = ko.observable();
        self.lastPriceUsdDisplay = ko.computed(function () {
            return self.lastPriceUsd();
        }).extend({ numeric: 2 });
        self._24hChangePercent = ko.observable();
        self._24hChangePercentDisplay = ko.computed(function () {
            return self._24hChangePercent();
        }).extend({ numeric: 2, displayNA: '' });
        self._24hVolume = ko.observable();
        self._24hVolumeDisplay = ko.computed(function () {
            return self._24hVolume();
        }).extend({ numeric: nrOfDecimals, displayNA: '' });
        self._24hQuantity = ko.observable();
        self._24hQuantityDisplay = ko.computed(function () {
            return self._24hQuantity();
        }).extend({ numeric: nrOfDecimals, displayNA: '' });

        ko.mapping.fromJS(data, {}, self);
    }

    ko.mapping.fromJS(data, {
        create: function (options) {
            return new _market(options.data);
        }
    }, root.allMarkets);
    _changeMarketTo();
};

var newModel;
function initViewModel(jsonData, nrOfDecimals) {
    var data = JSON.parse(jsonData);
    newModel = new viewModel(data, nrOfDecimals);
    ko.applyBindings(newModel);
}
﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

String.prototype.format = function () {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k]);
    }
    return a;
}

var Utils = (function () {
    function copyText(text, func) {
        var txt = document.createElement('input');
        document.body.appendChild(txt);
        txt.value = text; // chrome uses this
        txt.textContent = text; // FF uses this
        var sel = getSelection();
        var range = document.createRange();
        range.selectNode(txt);
        sel.removeAllRanges();
        sel.addRange(range);
        if (document.execCommand('copy') && func) {
            func();
        }
        else {
            alert('not..');
        }
        document.body.removeChild(txt);
    }

    function clearErrors(formId) {
        // get the form inside we are working - change selector to your form as needed
        var $form = $('#' + formId);

        // get validator object
        var $validator = $form.validate();

        // get errors that were created using jQuery.validate.unobtrusive
        var $errors = $form.find(".field-validation-error span");

        // trick unobtrusive to think the elements were succesfully validated
        // this removes the validation messages
        $errors.each(function () { $validator.settings.success($(this)); })

        // clear errors from validation
        $validator.resetForm();
    }
    var loading = (function () {
        return {
            Show: function () {
                $('body').loadingModal({
                    position: 'auto',
                    text: '',
                    color: '#fff',
                    opacity: '0.7',
                    backgroundColor: 'rgb(0,0,0)',
                    animation: 'fadingCircle'
                });
            },
            Hide: function () {
                $('body').loadingModal('destroy');
            }
        };
    })();

    var fromServerTimestamp = function (input) {
        var date = new Date(input * 1000);
        //var timelineOffset = new Date().getTimezoneOffset();
        //return new Date(date.getTime() - timelineOffset * 60000);
        return new Date(date.getTime());
    };

    var formatOrderTime = function (input) {
        var date = fromServerTimestamp(input);
        
        return {
            time: '{0}:{1}:{2}'.format(padDateUnit(date.getHours()), padDateUnit(date.getMinutes()), padDateUnit(date.getSeconds())),
            date: '{0}.{1}.{2}'.format(padDateUnit(date.getUTCDate()), padDateUnit(date.getUTCMonth() + 1), date.getUTCFullYear())
        };
    };
    function padDateUnit(input) {
        return input.toString().padStart(2, '0');
    }

    var notification = function () {
        var timeOut = 5000;
        return {
            Info: function (message) {
                iziToast.info({
                    //     title: 'Hello',
                    message: message,
                    timeout: timeOut
                });
            },
            Success: function (message) {
                iziToast.success({
                    //    title: 'OK',
                    message: message,
                    timeout: timeOut
                });

            },
            Warning: function (message) {
                iziToast.warning({
                    //     title: 'Caution',
                    message: message,
                    timeout: timeOut
                });
            },
            Error: function (message) {
                iziToast.error({
                    //      title: 'Error',
                    message: message,
                    timeout: timeOut
                });
            }
        };
    }();
    var isNullOrUndefined = function (input) {
        return input === null || input === undefined;
    };
    function toFixed(num, fixed) {
        fixed = fixed || 0;
        fixed = Math.pow(10, fixed);
        return Math.floor(num * fixed) / fixed;
    }

    return {
        CopyText: copyText,
        ClearErrors: clearErrors,
        Loading: loading,
        FromServerTimestamp: fromServerTimestamp,
        FormatOrderTime: formatOrderTime,
        Notification: notification,
        IsNullOrUndefined: isNullOrUndefined,
        ToFixed: toFixed
    };
})();


ko.extenders.numericSign = function (target) {
    var result = ko.dependentObservable({
        read: function () {
            var value = target();
            if (value === undefined) {
                return;
            }
            if (parseFloat(value) > 0) {
                return '+' + value;
            }
            return value;
        },
        write: target
    });

    result.raw = target;
    return result;
};
ko.extenders.numeric = function (target, precision) {
    var result = ko.dependentObservable({
        read: function () {
            if (target() === undefined) {
                return;
            }
            var floatValue = parseFloat(target());
            if (isNaN(floatValue)) {
                return;
            }
            return floatValue.toFixed(precision);
        },
        write: target
    });

    result.raw = target;
    return result;
};
ko.extenders.displayNA = function (target) {
    var result = ko.dependentObservable({
        read: function () {
            if (target() === undefined || target() === null) {
                return '-';
            }
            return target();
        },
        write: target
    });

    result.raw = target;
    return result;
};
ko.extenders.formatOrderTime = function (target) {
    var result = ko.dependentObservable({
        read: function () {
            if (target() === undefined || target() === null) {
                return '';
            }
            var unixTime = parseInt(target());
            return Utils.FormatOrderTime(unixTime);
        },
        write: target
    });

    result.raw = target;
    return result;
};
ko.extenders.changePriceCss = function (target) {
    var result = ko.dependentObservable({
        read: function () {
            if (target() === undefined || target() === null) {
                return '';
            }
            if (!target()) {
                return "";
            }
            var diff = parseFloat(target());
            if (diff === 0) {
                return "";
            }
            if (diff > 0) {
                return "color-green";
            }
            return "color-red";
        },
        write: target
    });

    result.raw = target;
    return result;
};

ko.bindingHandlers['undefinedVisible'] = {
    'update': function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var isCurrentlyVisible = !(element.style.display == "none");
        if ((value !== undefined && value !== null) && !isCurrentlyVisible)
            element.style.display = "";
        else if ((value === undefined || value === null) && isCurrentlyVisible)
            element.style.display = "none";
    }
};
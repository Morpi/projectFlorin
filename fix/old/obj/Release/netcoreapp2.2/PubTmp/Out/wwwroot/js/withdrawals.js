﻿var DataServices = new function () {
    var unknownError = function () {
        return {
            error: true,
            message: "Unknown error occurred"
        };
    }();
    function getOrders(filter) {
        var dfd = jQuery.Deferred();
        Utils.Loading.Show();
        var url = '/Withdrawals/Get';
        $.get(url, filter, function (result) {
            Utils.Loading.Hide();
            dfd.resolve(result);
        }).fail(function () {
            dfd.resolve(unknownError);
        });
        return dfd.promise();
    }

    return {
        GetOrders: getOrders
    };
};
var viewModel = function (data, nrOfDecimals) {
    var root = this;
    root.withdrawals = ko.observableArray();
    root.count = ko.observable();
    root.currencies = ko.observableArray();
    root.showExceedMessage = ko.computed(function () {
        root.count() !== undefined && parseInt(root.count()) > 100;
    });
    root.startDate = ko.observable();
    root.endDate = ko.observable();

    function _withdrawal(data) {
        var self = this;

        self.id = ko.observable();
        self.dateAndTime = ko.observable();
        self.dateAndTimeDisplay = ko.computed(function () {
            return self.dateAndTime();
        }).extend({ formatOrderTime: '' });
        self.from = ko.observable();
        self.to = ko.observable();
        self.type = ko.observable();
        self.price = ko.observable();
        self.priceDisplay = ko.computed(function () {
            return self.price();
        }).extend({ numeric: nrOfDecimals });
        self.quantity = ko.observable();
        self.quantityDisplay = ko.computed(function () {
            return self.quantity();
        }).extend({ numeric: nrOfDecimals });
        self.address = ko.observable();
        self.volume = ko.computed(function () {
            return parseFloat(self.price()) * parseFloat(self.quantity());
        }).extend({ numeric: nrOfDecimals });
        self.filled = ko.observable().extend({ numeric: nrOfDecimals });

        self.getImage = function (code) {
            return '/img/currencies/{0}.png'.format(code);
        };
        self.name = ko.observable();
        ko.mapping.fromJS(data, {}, self);
    }
    root.selectedCurrency = ko.observable();
    root.filter = function () {
        var filterObj = {};
        var startDate = tryParseDate(root.startDate());
        if (startDate) {
            filterObj.startDate = startDate.toISOString();
        }
        var endDate = tryParseDate(root.endDate());
        if (endDate) {
            filterObj.endDate = endDate.toISOString();
        }
        if (root.selectedCurrency()) {
            filterObj.selectedCurrency = root.selectedCurrency().code();
        }
        DataServices.GetOrders(filterObj).then(function (result) {
            if (result.error) {
                Utils.Notification.Error(result.message);
                return;
            }
            applyFromJS(result.data);
        });
    };
    function tryParseDate(input) {
        try {
            return $.datepicker.parseDate('mm/dd/yy', input);
        } catch (e) {
            return null;
        }
    }

    function applyFromJS(input) {
        ko.mapping.fromJS(input, {
            'withdrawals': {
                create: function (options) {
                    return new _withdrawal(options.data);
                }
            }
        }, root);
    }
    applyFromJS(data);
};

var newModel;
function initViewModel(jsonData) {
    if (jsonData) {
        var data = JSON.parse(jsonData);

        newModel = new viewModel(data, data.nrOfDecimals);
    }
    else {
        newModel = new viewModel();
    }

    ko.applyBindings(newModel);
}
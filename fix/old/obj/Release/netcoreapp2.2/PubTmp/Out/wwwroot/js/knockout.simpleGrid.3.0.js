﻿(function () {
    // Private function
    function getColumnsForScaffolding(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ name: propertyName, headerText: propertyName, rowText: propertyName });
        }
        return columns;
    }

    ko.grid = {
        // Defines a view model class you can use to populate a grid
        viewModel: function (configuration) {
            var self = this;

            self.data = configuration.data;
            self.currentPageIndex = ko.observable(0);
            self.pageSize = configuration.pageSize;// || 10000;

            // If you don't specify columns configuration, we'll use scaffolding
            self.columns = configuration.columns || getColumnsForScaffolding(ko.unwrap(self.data));

            self.itemsOnCurrentPage = ko.computed(function () {
                if (!self.pageSize) {
                    return ko.unwrap(self.data);
                }
                var startIndex = self.pageSize * self.currentPageIndex();
                return ko.unwrap(self.data).slice(startIndex, startIndex + self.pageSize);
            });

            self.maxPageIndex = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                return Math.ceil(ko.unwrap(self.data).length / self.pageSize) - 1;
            });


            self.sort = function (item) {
                if (!item.sortable) {
                    return;
                }
                var columnName = item.name;
                if (self.sortedColumn() !== columnName) {
                    self.ascendingOrder(true);
                }
                else {
                    self.ascendingOrder(!self.ascendingOrder());
                }

                self.sortedColumn(columnName);

                configuration.data.sort(function (first, second) {
                    if (self.ascendingOrder()) {
                        return first[columnName] < second[columnName] ? -1 : 1;
                    }
                    return first[columnName] > second[columnName] ? -1 : 1;
                });
            };
            self.sortedColumn = ko.observable();
            self.ascendingOrder = ko.observable();
            self.isSorted = function (item) {
                return self.sortedColumn() === item.name;
            };
            self.isSortedUp = function () {
                return self.ascendingOrder();
            };

            self.startRange = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                if (self.currentPageIndex() === 0) {
                    return self.currentPageIndex() + 1;
                } else {
                    return self.currentPageIndex() * self.pageSize + 1;
                }
            });
            self.endRange = ko.computed(function () {
                if (!self.pageSize) {
                    return;
                }
                if (self.currentPageIndex() === 0) {
                    if (self.pageSize < ko.unwrap(self.data).length) {
                        return self.pageSize;
                    } else {
                        return ko.unwrap(self.data).length;
                    }
                } else if (self.currentPageIndex() === self.maxPageIndex()) {
                    return ko.unwrap(self.data).length;
                } else {
                    return (self.currentPageIndex() + 1) * pageSize;
                }
            });
            self.moveToPrevious = function () {
                if (self.currentPageIndex() === 0) {
                    return false;
                }
                self.currentPageIndex(self.currentPageIndex() - 1);
            };
            self.moveToNext = function () {
                if (self.currentPageIndex() === self.maxPageIndex()) {
                    return false;
                }
                self.currentPageIndex(self.currentPageIndex() + 1);
            };
            self.deleteItem = function (item) {
                configuration.itemDelete(item);
            };
        }
    };

    // Templates used to render the grid
    var templateEngine = new ko.nativeTemplateEngine();

    templateEngine.addTemplate = function (templateName, templateMarkup) {
        document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
    };

    templateEngine.addTemplate("ko_grid_grid", "\
             <table class=\"ko-grid\" cellspacing=\"0\">\
                <thead>\
                    <tr data-bind=\"foreach: columns\">\
                        <th data-bind=\"style:{cursor:$data.sortable?'pointer':'default'},click:function(){$parent.sort($data);},css:$data.cssClass\">\
                            <span data-bind=\"html: typeof headerText == 'function' ? headerText($data) : headerText\"></span>\
                            <div data-bind=\"visible: $data.sortable && $parent.isSorted($data)\">\
                                <span data-bind=\"visible: $parent.isSortedUp()\">\
                                    Up\
                                </span>\
                                <span data-bind=\"visible:!$parent.isSortedUp()\">\
                                    Down\
                                </span>\
                            </div>\
                            <div data-bind=\"visible: $data.sortable && !$parent.isSorted($data)\">\
                                UpDown\
                            </div>\
                        </th>\
                    </tr>\
                </thead>\
                <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                    <tr data-bind=\"foreach: $parent.columns\">\
                        <!-- ko if: $data.deleteAction -->\
                             <td><a href =\"javascript:void(0)\" data-bind=\"click:function(){$root.deleteItem($parent);}\">delete</a></td>\
                        <!-- /ko -->\
                        <!-- ko if: !$data.deleteAction -->\
                            <td data-bind=\"html: typeof rowText == 'function' ? rowText($parent) : $parent[name] \"></td>\
                        <!-- /ko -->\
                    </tr>\
                </tbody>\
            </table>");

    templateEngine.addTemplate("ko_grid_pageLinks", "\
                    <div class='ko-grid-pageLinks' data-bind='visible:pageSize' >\
                        <span>Showing <span data-bind='text: startRange'></span>-<span data-bind='text: endRange'></span> of <span data-bind='text: ko.unwrap(data).length'></span> items</span>\
                        <div class='pull-right'>\
                          <nav>\
                             <ul class='pager'>\
                                <li class='active-pager' data-bind='css:{disabled:currentPageIndex()===0},click: moveToPrevious'><-<a href='#' class='glyphicon windows-icon-chevron-left' aria-hidden='true' aria-label='previous'></a></li>\
                                <li class='active-pager' data-bind='css:{disabled:currentPageIndex()=== maxPageIndex()},click: moveToNext'>-><a href='#' class='glyphicon windows-icon-chevron-right' aria-hidden='true' aria-label='next'></a></li>\
                             </ul>\
                          </nav>\
                       </div>\
                    </div >");

    // The "grid" binding
    ko.bindingHandlers.grid = {
        init: function () {
            return { 'controlsDescendantBindings': true };
        },
        // This method is called to initialize the node, and will also be called again if you change what the grid is bound to
        update: function (element, viewModelAccessor, allBindings) {
            var viewModel = viewModelAccessor();

            // Empty the element
            while (element.firstChild)
                ko.removeNode(element.firstChild);

            // Allow the default templates to be overridden
            var gridTemplateName = allBindings.get('gridTemplate') || "ko_grid_grid",
                pageLinksTemplateName = allBindings.get('gridPagerTemplate') || "ko_grid_pageLinks";

            // Render the main grid
            var gridContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");

            // Render the page links
            var pageLinksContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(pageLinksTemplateName, viewModel, { templateEngine: templateEngine }, pageLinksContainer, "replaceNode");
        }
    };
})();
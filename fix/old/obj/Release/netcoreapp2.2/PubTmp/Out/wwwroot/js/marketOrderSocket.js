﻿var MarketOrderSocket = function () {
    var self = this;
    function connect(settings) {
        self.connection = new signalR.HubConnectionBuilder()
            .withUrl('https://localhost:60000/marketOrder')
            .configureLogging(signalR.LogLevel.Information)
            .build();
        var listeners = settings.listeners;
        self.connection.on('Test',
            function (result) {
                alert(result);
            });
        self.connection.on('RemoveOrder',
            function (result) {
                listeners.onRemoveOrder(result);
            });
        self.connection.on('UserRemoveOrder',
            function (result) {
                listeners.onUserRemoveOrder(result);
            });
        self.connection.on('AddMarketOrder',
            function (result) {
                listeners.onAddMarketOrder(result);
            });
        self.connection.on('AddCurrentUserOrder',
            function (result) {
                listeners.onAddCurrentUserOrder(result);
            });
        self.connection.on('AddOtherUserOrder',
            function (result) {
                listeners.onAddOtherUserOrder(result);
            });
        self.connection.on('MenuChange',
            function (result) {
                listeners.onMenuChange(result);
            });
        self.connection.on('ChangeUSDPrice',
            function (result) {
                listeners.onChangeUSDPrice(result);
            });
        
        self.connection.on('Init',
            function (result) {
                listeners.onInit(result);
            });
        self.connection.start().then(function () {
            self.connection.invoke('JoinMarket', settings.marketCode);
        });
    }
    return {
        Connect: connect
    };
}();
﻿var viewModel = function (data) {
    var root = this;
    root.searchTerm = ko.observable();
    root.allCurrencies = ko.observableArray();

    root.currencies = ko.computed(function () {
        return ko.utils.arrayFilter(root.allCurrencies(), function (currency) {
            var searchTerm = root.searchTerm();
            if (!searchTerm) {
                return true;
            }
            searchTerm = searchTerm.toLowerCase().trim();
            return currency.code().toLowerCase().includes(searchTerm) || currency.name().toLowerCase().includes(searchTerm);
        });
    }, root).extend({ rateLimit: 200 });
  

    function _currency(data) {
        var self = this;

        self.code = ko.observable();
        self.name = ko.observable();
        self.tradingFee = ko.observable();
        self.minimumWithdrawal = ko.observable();
        self.maximumWithdrawal = ko.observable();
        self.maximumWithdrawalDisplay = ko.computed(function () {
            if (!self.maximumWithdrawal()) {
                return '-';
            }
            return self.maximumWithdrawal();
        });
        self.withdrawalFee = ko.observable();
        self.depositFee = ko.observable();

        self.getImage = function (data) {
            return '/img/currencies/{0}.png'.format(data.code());
        };

        ko.mapping.fromJS(data, {}, self);
    }

    ko.mapping.fromJS(data, {
        'allCurrencies': {
            create: function (options) {
                return new _currency(options.data);
            }
        }
    }, root);
};

var newModel;
function initViewModel(jsonData) {
    var data = JSON.parse(jsonData);
    newModel = new viewModel(data);
    ko.applyBindings(newModel);
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Models
{
    public class WithdrawModel
    {
        public string Address { get; set; }

        public decimal Amount { get; set; }

        public string Code { get; set; }
    }
}

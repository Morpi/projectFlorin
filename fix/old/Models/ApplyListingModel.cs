﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebUI.Attributes;

namespace WebUI.Models
{
    public class ApplyListingModel
    {
        [Required]
        [DisplayName("Contact Email")]
        [EmailAddress]
        [MaxLength(200)]
        public string ContactEmail { get; set; }

        [Required]
        [DisplayName("Currency Name")]
        [MaxLength(200)]
        public string CurrencyName { get; set; }

        [Required]
        [DisplayName("Currency Symbol")]
        [MaxLength(10)]
        public string CurrencySymbol { get; set; }

        [Required]
        [DisplayName("Website Url")]
        [MaxLength(200)]
        public string WebsiteUrl { get; set; }
        
        [DisplayName("Blockchain Explorer")]
        [MaxLength(200)]
        public string BlockchainExplorer { get; set; }

        [Required]
        [MaxLength(200)]
        public string Twitter { get; set; }
        
        [DisplayName("Other Information")]
        [MaxLength(500)]
        public string OtherInformation { get; set; }
        
        [GoogleReCaptchaValidation]
        [BindProperty(Name = "g-recaptcha-response")]
        public String GoogleReCaptchaResponse { get; set; }
    }
}

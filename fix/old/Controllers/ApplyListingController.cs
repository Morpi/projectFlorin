﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Route("apply-listing")]
    public class ApplyListingController : Controller
    {
        private readonly ExchangeContext _dbContext;
        private readonly LogRepository _logger;

        public ApplyListingController(ExchangeContext dbContext, LogRepository logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Index(ApplyListingModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                _dbContext.ApplyListings.Add(new Core.Storage.Tables.ApplyListing()
                {
                    ContactEmail = model.ContactEmail,
                    CurrencyName = model.CurrencyName,
                    CurrencySymbol = model.CurrencySymbol,
                    WebsiteUrl = model.WebsiteUrl,
                    BlockchainExplorer = model.BlockchainExplorer,
                    Twitter = model.Twitter,
                    OtherInformation = model.OtherInformation,
                    CreatedAt = DateTime.UtcNow
                });
                _dbContext.SaveChanges();

                var message = "Thanks for making the request for listing your currency on our platform! We will get in touch with you shortly.";
                ViewData["Message"] = message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.ContactControllerIndex, ex);
                ModelState.AddModelError(string.Empty, "Unknown error occurred");
                return View();
            }
        }
    }
}
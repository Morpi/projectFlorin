﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Api;
using Core.Cache;
using Core.Storage;
using Core.Storage.Repositories;
using Core.Storage.Tables;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebUI.Helpers;

namespace WebUI.Controllers
{
    [Authorize]
    public class HistoryOrdersController : ExchangeControllerBase
    {
        private readonly ExchangeCache _cache;
        private readonly ExchangeContext _dbContext;
        private readonly ExchangeHelper _helper;
        private readonly LogRepository _logger;

        public HistoryOrdersController(ExchangeCache cache, ExchangeContext dbContext, ExchangeHelper helper, LogRepository logger)
        {
            _cache = cache;
            _dbContext = dbContext;
            _helper = helper;
            _logger = logger;
        }

        class FilteredOrders
        {
            public object PendingOrders { get; set; }

            public int Count { get; set; }
        }

        private FilteredOrders GetFilteredOrders(Filter filter = null)
        {
            var markets = _cache.LimitedMarkets;

            var pendingOrdersQuery = (from fo in _dbContext.FilledOrders
                                      where (fo.BuyerId == UserId || fo.SellerId == UserId)
                                      select fo);
            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter.SelectedCurrency))
                {
                    pendingOrdersQuery = pendingOrdersQuery
                        .Join(markets, fo => fo.MarketCode, m => m.Code, (fo, m) => new { filledOrder = fo, market = m })
                        .Where(item => item.market.From == filter.SelectedCurrency || item.market.To == filter.SelectedCurrency)
                        .Select(item => item.filledOrder);
                }
                if (filter.StartDate.HasValue)
                {
                    pendingOrdersQuery = pendingOrdersQuery
                     .Where(fo => fo.CreatedAt >= filter.StartDate.Value.Date);
                }
                if (filter.EndDate.HasValue)
                {
                    pendingOrdersQuery = pendingOrdersQuery
                     .Where(fo => fo.CreatedAt <= filter.EndDate.Value.Date.AddDays(1).AddSeconds(-1));
                }
            }
            pendingOrdersQuery = pendingOrdersQuery.OrderByDescending(c => c.CreatedAt);

            var pendingOrders = pendingOrdersQuery
                .Take(100)
                .ToList()
                .Select(c =>
                {
                    var market = markets.Single(m => m.Code == c.MarketCode);
                    var result = new
                    {
                        id = c.Id,
                        marketCode = c.MarketCode,
                        dateAndTime = _helper.ToUnixTime(c.CreatedAt),
                        from = market.From,
                        to = market.To,
                        type = c.Type.ToString(),
                        price = c.Price,
                        quantity = c.Quantity
                    };
                    return result;
                });
            return new FilteredOrders()
            {
                PendingOrders = pendingOrders,
                Count = pendingOrdersQuery.Count()
            };
        }

        [Route("history-orders")]
        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var currencies = _cache.Currencies
                .OrderBy(c => c.Title)
                .Select(c => new
                {
                    code = c.Code,
                    name = $"{c.Title} ({c.Code})"
                });
                var filteredOrders = GetFilteredOrders();

                var data = JsonConvert.SerializeObject(new
                {
                    pendingOrders = filteredOrders.PendingOrders,
                    currencies = currencies,
                    count = filteredOrders.Count,
                    nrOfDecimals = 8
                });
                return View((object)data);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerIndex, ex);
                return View();
            }
        }

        [HttpGet]
        public ApiResult GetOrders(Filter filter)
        {
            try
            {
                var result = GetFilteredOrders(filter);
                return ApiResult.GetSuccessResult(result);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerGetOrders, ex);
                return ApiResult.GetErrorResult(ExchangeConsts.GENERIC_ERROR);
            }
        }

        public class Filter
        {
            public string SelectedCurrency { get; set; }

            public DateTime? StartDate { get; set; }

            public DateTime? EndDate { get; set; }
        }
    }
}
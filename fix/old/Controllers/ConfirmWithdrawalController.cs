﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Security;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class ConfirmWithdrawalController : Controller
    {
        private readonly EncryptionService _encryptionService;
        private readonly ExchangeContext _dbContext;
        private readonly LogRepository _logger;

        public ConfirmWithdrawalController(EncryptionService encryptionService, ExchangeContext dbContext, LogRepository logger)
        {
            _encryptionService = encryptionService;
            _dbContext = dbContext;
            _logger = logger;
        }

        [Route("confirm-withdrawal")]
        public IActionResult Index(string code)
        {
            try
            {
                var withdrawId = _encryptionService.Decrypt(code);
                var guidWithdrawId = Guid.Parse(withdrawId);
                var withdraw = _dbContext.Withdraws.FirstOrDefault(w => w.Id == guidWithdrawId);

                if (withdraw == null || withdraw.Status != WithdrawStatus.UnConfirmedEmail)
                {
                    ViewData["Message"] = "Withdrawal already confirmed or expired.";
                    return View();
                }
                withdraw.Status = WithdrawStatus.UnProcessed;
                _dbContext.SaveChanges();
                ViewData["Message"] = "Withdrawal has been successfully confirmed.";
            }
            catch (Exception ex)
            {
                _logger.InsertCritical(LogCategory.ConfirmWithdrawal, ex);
                ViewData["Message"] = ExchangeConsts.GENERIC_ERROR;
            }
            return View();
        }
    }
}
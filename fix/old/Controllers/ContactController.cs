﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Mvc;
using WebUI.Helpers;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class ContactController : Controller
    {
        private readonly ExchangeContext _dbContext;
        private readonly LogRepository _logger;

        public ContactController(ExchangeContext dbContext, LogRepository logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(ContactModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                _dbContext.ContactEntries.Add(new Core.Storage.Tables.ContactEntry()
                {
                    Subject = model.Subject,
                    FullName = model.FullName,
                    Email = model.Email,
                    Content = model.Content,
                    CreatedAt = DateTime.UtcNow
                });
                _dbContext.SaveChanges();

                var message = "Thanks for contacting us! We will get in touch with you shortly.";
                ViewData["Message"] = message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.ContactControllerIndex, ex);
                ModelState.AddModelError(string.Empty, "Unknown error occurred");
                return View();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Cache;
using Core.Storage;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebUI.Controllers
{
    public class MarketsController : Controller
    {
        private readonly ExchangeCache _cache;
        private readonly ExchangeContext _dbContext;

        public MarketsController(ExchangeCache cache, ExchangeContext dbContext)
        {
            _cache = cache;
            _dbContext = dbContext;
        }
        
        public IActionResult Index()
        {
            var currencies = _cache.Currencies;
            var markets = _dbContext.Markets.ToList();

            var data = markets.Select(m => new
            {
                from = m.From,
                to = m.To,
                currencyName = currencies.Single(c => c.Code == m.To).Title,
                code = m.Code,
                lastPrice = m.LastPrice,
                lastPriceUsd = currencies.Single(c => c.Code == m.From).PriceUsd * m.LastPrice,
                _24hChangePercent = (m.LastPrice / m.Last24hPrice - 1) * 100,
                _24hVolume = m._24hVolume,
                _24hQuantity = m._24hQuantity
            });
            ViewBag.JsonData = JsonConvert.SerializeObject(data);
            ViewBag.NrOfDecimals = 8;

            return View();
        }
    }
}
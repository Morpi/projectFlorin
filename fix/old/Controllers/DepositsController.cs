﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Cache;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebUI.Helpers;
using Newtonsoft.Json;
using Core.Api;

namespace WebUI.Controllers
{
    [Authorize]
    public class DepositsController : ExchangeControllerBase
    {
        private readonly ExchangeCache _cache;
        private readonly ExchangeContext _dbContext;
        private readonly ExchangeHelper _helper;
        private readonly LogRepository _logger;

        public DepositsController(ExchangeCache cache, ExchangeContext dbContext, ExchangeHelper helper, LogRepository logger)
        {
            _cache = cache;
            _dbContext = dbContext;
            _helper = helper;
            _logger = logger;
        }

        class FilteredDeposit
        {
            public object Deposits { get; set; }

            public int Count { get; set; }
        }

        private FilteredDeposit GetFilteredDeposits(Filter filter = null)
        {
            var currencies = _cache.Currencies;

            var depositsQuery = (from d in _dbContext.Deposits
                                 join ua in _dbContext.UserAddresses on d.To equals ua.Address
                                 where ua.UserId == UserId
                                 select d);
            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter.SelectedCurrency))
                {
                    depositsQuery = depositsQuery
                        .Where(d => d.CurrencyCode == filter.SelectedCurrency);
                }
                if (filter.StartDate.HasValue)
                {
                    depositsQuery = depositsQuery
                     .Where(d => d.CreatedAt >= filter.StartDate.Value.Date);
                }
                if (filter.EndDate.HasValue)
                {
                    depositsQuery = depositsQuery
                     .Where(d => d.CreatedAt <= filter.EndDate.Value.Date.AddDays(1).AddSeconds(-1));
                }
            }
            depositsQuery = depositsQuery.OrderByDescending(c => c.CreatedAt);

            var deposits = depositsQuery
                .Take(100)
                .ToList()
                .Select(d =>
                {
                    var currency = currencies.Single(c => c.Code == d.CurrencyCode);
                    return new
                    {
                        status = d.Status == DepositStatus.DoneWithSuccess ? "Confirmed" : (d.Status == DepositStatus.ConfirmedExternallyWithError || d.Status == DepositStatus.DoneWithError ? "Error" : "On Pending"),
                        code = d.CurrencyCode,
                        quantity = d.Amount,
                        dateAndTime = _helper.ToUnixTime(d.CreatedAt),
                        transactionUrl = string.Format(currency.TransactionUrl, d.TransactionHash),
                        name = currency.Title
                    };
                });
            return new FilteredDeposit()
            {
                Deposits = deposits,
                Count = depositsQuery.Count()
            };
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var currencies = _cache.Currencies
                .OrderBy(c => c.Title)
                .Select(c => new
                {
                    code = c.Code,
                    name = $"{c.Title} ({c.Code})"
                });
                var deposits = GetFilteredDeposits();

                var data = JsonConvert.SerializeObject(new
                {
                    deposits = deposits.Deposits,
                    currencies = currencies,
                    count = deposits.Count,
                    nrOfDecimals = 8
                });
                return View((object)data);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerIndex, ex);
                return View();
            }
        }

        [HttpGet]
        public ApiResult Get(Filter filter)
        {
            try
            {
                var result = GetFilteredDeposits(filter);
                return ApiResult.GetSuccessResult(result);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerGetOrders, ex);
                return ApiResult.GetErrorResult(ExchangeConsts.GENERIC_ERROR);
            }
        }

        public class Filter
        {
            public string SelectedCurrency { get; set; }

            public DateTime? StartDate { get; set; }

            public DateTime? EndDate { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Cache;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebUI.Helpers;
using Newtonsoft.Json;
using Core.Api;
using Core.Storage.Tables;

namespace WebUI.Controllers
{
    [Authorize]
    public class WithdrawalsController : ExchangeControllerBase
    {
        private readonly ExchangeCache _cache;
        private readonly ExchangeContext _dbContext;
        private readonly ExchangeHelper _helper;
        private readonly LogRepository _logger;

        public WithdrawalsController(ExchangeCache cache, ExchangeContext dbContext, ExchangeHelper helper, LogRepository logger)
        {
            _cache = cache;
            _dbContext = dbContext;
            _helper = helper;
            _logger = logger;
        }

        class Filteredwithdrawal
        {
            public object Withdrawals { get; set; }

            public int Count { get; set; }
        }

        private Filteredwithdrawal GetFilteredwithdrawals(Filter filter = null)
        {
            var currencies = _cache.Currencies;

            var withdrawalsQuery = (from w in _dbContext.Withdraws
                                 join ua in _dbContext.UserAddresses on w.From equals ua.Address
                                 where ua.UserId == UserId
                                 select w);
            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter.SelectedCurrency))
                {
                    withdrawalsQuery = withdrawalsQuery
                        .Where(d => d.CurrencyCode == filter.SelectedCurrency);
                }
                if (filter.StartDate.HasValue)
                {
                    withdrawalsQuery = withdrawalsQuery
                     .Where(d => d.CreatedAt >= filter.StartDate.Value.Date);
                }
                if (filter.EndDate.HasValue)
                {
                    withdrawalsQuery = withdrawalsQuery
                     .Where(d => d.CreatedAt <= filter.EndDate.Value.Date.AddDays(1).AddSeconds(-1));
                }
            }
            withdrawalsQuery = withdrawalsQuery.OrderByDescending(c => c.CreatedAt);

            var withdrawals = withdrawalsQuery
                .Take(100)
                .ToList()
                .Select(w =>
                {
                    var currency = currencies.Single(c => c.Code == w.CurrencyCode);
                    var confirmationText = GetConfirmationText(w);

                    return new
                    {
                        status = confirmationText,
                        code = w.CurrencyCode,
                        quantity = w.Amount,
                        address = w.To,
                        dateAndTime = _helper.ToUnixTime(w.CreatedAt),
                        transactionUrl = !string.IsNullOrEmpty(w.TransactionHash) ?
                            string.Format(currency.TransactionUrl, w.TransactionHash) :
                            null,
                        name = currency.Title
                    };
                });
            return new Filteredwithdrawal()
            {
                Withdrawals = withdrawals,
                Count = withdrawalsQuery.Count()
            };
        }

        private object GetConfirmationText(Withdraw withdraw)
        {
            if (withdraw.Status == WithdrawStatus.RejectedByKnownIssue)
            {
                return $"Rejected ({withdraw.RejectedReason})";
            }
            if (withdraw.Status == WithdrawStatus.DoneWithSuccess)
            {
                return "Confirmed";
            }
            if (withdraw.Status == WithdrawStatus.UnConfirmedEmail)
            {
                return "Waiting For Confirmation";
            }
            return "On Pending";
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var currencies = _cache.Currencies
                .OrderBy(c => c.Title)
                .Select(c => new
                {
                    code = c.Code,
                    name = $"{c.Title} ({c.Code})"
                });
                var withdrawals = GetFilteredwithdrawals();

                var data = JsonConvert.SerializeObject(new
                {
                    withdrawals = withdrawals.Withdrawals,
                    currencies = currencies,
                    count = withdrawals.Count,
                    nrOfDecimals = 8
                });
                return View((object)data);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerIndex, ex);
                return View();
            }
        }

        [HttpGet]
        public ApiResult Get(Filter filter)
        {
            try
            {
                var result = GetFilteredwithdrawals(filter);
                return ApiResult.GetSuccessResult(result);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.HistoryOrdersControllerGetOrders, ex);
                return ApiResult.GetErrorResult(ExchangeConsts.GENERIC_ERROR);
            }
        }

        public class Filter
        {
            public string SelectedCurrency { get; set; }

            public DateTime? StartDate { get; set; }

            public DateTime? EndDate { get; set; }
        }
    }
}
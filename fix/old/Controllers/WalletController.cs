﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Api;
using Core.Cache;
using Core.Storage;
using Core.Storage.Repositories;
using Core.Wallets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebUI.Helpers;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class WalletController : ExchangeControllerBase
    {
        private readonly ExchangeContext _dbContext;
        private readonly WalletHelper _walletHelper;
        private readonly LogRepository _logger;
        private readonly ExchangeCache _cache;
        private readonly ExchangeHelper _helper;

        public WalletController(WalletHelper walletHelper, ExchangeContext dbContext, LogRepository logger, ExchangeCache cache, ExchangeHelper helper)
        {
            _walletHelper = walletHelper;
            _dbContext = dbContext;
            _logger = logger;
            _cache = cache;
            _helper = helper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var userId = base.UserId;
            var curenciesWithBalances = new List<object>();
            var userBalances = _dbContext.UserBalances.Where(ub => ub.UserId == userId).ToList();
            var pendingWitdrawls =
                (from w in _dbContext.Withdraws
                 join wa in _dbContext.UserAddresses on w.From equals wa.Address
                 where wa.UserId == userId && w.Status != WithdrawStatus.DoneWithSuccess
                 select w).ToList();
            var markets = _dbContext.Markets.ToList();
            var currencies = _cache.Currencies;

            var pendingOrders = (from po in _dbContext.PendingOrders
                                 where po.UserId == userId
                                 select po).ToList();

            var btcPriceUsd = _dbContext.Currencies.Single(c => c.Code == ExchangeConsts.BITCOIN_CODE).PriceUsd.Value;
            
            var ethPriceInBTC = 0m;
            var lastBTCPrice = markets.SingleOrDefault(m => m.From == ExchangeConsts.BITCOIN_CODE && m.To == ExchangeConsts.ETHEREUM_CODE);
            if (lastBTCPrice != null && lastBTCPrice.LastPrice.HasValue)
            {
                ethPriceInBTC = lastBTCPrice.LastPrice.Value;
            }
            else
            {
                var ethPriceUsd = _dbContext.Currencies.Single(c => c.Code == ExchangeConsts.ETHEREUM_CODE).PriceUsd.Value;
                ethPriceInBTC = ethPriceUsd / btcPriceUsd;
            }

            var totalBtc = 0m;
            foreach (var currency in currencies.OrderBy(c => c.Title))
            {
                var currencyBalance = userBalances.Single(ub => ub.CurrencyCode == currency.Code);
                var pendingWithdrawls = pendingWitdrawls.Where(pw => pw.CurrencyCode == currency.Code).Sum(pw => pw.Amount);
                
                var onTrades = (from po in pendingOrders
                                join lm in markets on po.MarketCode equals lm.Code
                                where (lm.From == currency.Code && po.Type == OrderType.Buy) || (lm.To == currency.Code && po.Type == OrderType.Sell)
                                select po)
                                .Sum(po => _helper.GetLockedAmount(po));
                var btcValue = 0m;
                if (currency.Code == ExchangeConsts.BITCOIN_CODE)
                {
                    btcValue = currencyBalance.Amount;
                }
                else if (currency.Code == ExchangeConsts.ETHEREUM_CODE)
                {
                    btcValue = ethPriceInBTC * currencyBalance.Amount;
                }
                else
                {
                    lastBTCPrice = markets.SingleOrDefault(m => m.From == ExchangeConsts.BITCOIN_CODE && m.To == currency.Code);
                    var lastETHPrice = markets.SingleOrDefault(m => m.From == ExchangeConsts.ETHEREUM_CODE && m.To == currency.Code);
                    if (lastBTCPrice != null && lastBTCPrice.LastPrice.HasValue && lastETHPrice == null && lastETHPrice.LastPrice.HasValue)
                    {
                        btcValue = lastBTCPrice.LastPrice.Value * currencyBalance.Amount;
                    }
                    else if (lastBTCPrice == null && lastETHPrice != null && lastETHPrice.LastPrice.HasValue)
                    {
                        btcValue = ethPriceInBTC * lastETHPrice.LastPrice.Value * currencyBalance.Amount;
                    }
                    else if (lastBTCPrice != null && lastBTCPrice.LastPrice.HasValue && lastETHPrice != null && lastETHPrice.LastPrice.HasValue)
                    {
                        var bTCPrice = lastBTCPrice.LastPriceUpdated.Value > lastETHPrice.LastPriceUpdated.Value ? 
                            lastBTCPrice.LastPrice.Value : 
                            ethPriceInBTC * lastETHPrice.LastPrice.Value;

                        btcValue = bTCPrice * currencyBalance.Amount;
                    }
                }
                totalBtc += btcValue;

                curenciesWithBalances.Add(new
                {
                    code = currency.Code,
                    name = currency.Title,
                    available = currencyBalance.Amount - pendingWithdrawls - onTrades,
                    pendingWithdrawls = pendingWithdrawls,
                    onTrades = onTrades,
                    btcValue = btcValue,
                    usdValue = btcValue * btcPriceUsd,
                    fee = currency.WithdrawFee,
                    minDeposit = currency.MinDeposit,
                    minWithdrawal = currency.MinWithdrawal
                });
            }
            ViewBag.JsonData = JsonConvert.SerializeObject(new
            {
                btcValue = totalBtc,
                usdValue = totalBtc * btcPriceUsd,
                allCurrencies = curenciesWithBalances
            });
            return View();
        }

        [HttpPost]
        public string Withdraw(WithdrawModel model)
        {
            const string genericError = "Unexpected error. Please try again later";
            if (!ModelState.IsValid)
            {
                var firstError = ModelState.SelectMany(ms => ms.Value.Errors).Select(e => e.ErrorMessage).FirstOrDefault();
                return firstError ?? genericError;
            }

            string error = null;
            _walletHelper.Send(model.Code, UserId, model.Address, model.Amount, ref error);
            if (!string.IsNullOrEmpty(error))
            {
                return error;
            }
            return null;
        }

        [HttpGet]
        public ApiResult GetAddress(string code)
        {
            var result = _walletHelper.GetOrAssignAddress(base.UserId, code);
            return new ApiResult()
            {
                Data = result,
                Error = string.IsNullOrEmpty(result),
                Message = string.IsNullOrEmpty(result) ? "Couldn't retrieve address for " + code : null
            };
        }
    }
}
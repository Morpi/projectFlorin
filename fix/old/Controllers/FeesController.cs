﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Cache;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebUI.Controllers
{
    public class FeesController : Controller
    {
        private readonly ExchangeCache _cache;

        public FeesController( ExchangeCache cache)
        {
            _cache = cache;
        }
        
        public IActionResult Index()
        {
            var currencies = _cache.Currencies;
            var data = currencies.Select(c => new
            {
                code = c.Code,
                name = c.Title,
                tradingFee = _cache.ConfigurationSettings.TRANSACTION_FEE,
                minimumWithdrawal = c.MinWithdrawal,
                maximumWithdrawal = c.MaxWithdrawal,
                minimumDeposit = c.MinDeposit,
                withdrawalFee = c.WithdrawFee
            });
            ViewBag.JsonData = JsonConvert.SerializeObject(new
            {
                allCurrencies = data
            });
            return View();
        }
    }
}
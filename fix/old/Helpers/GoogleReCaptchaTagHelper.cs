﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace WebUI.Helpers
{
    public static class GoogleReCaptchaTagHelper
    {
        public static IHtmlContent GoogleReCaptcha(this IHtmlHelper htmlHelper, String siteKey, String callback = null)
        {
            var tagBuilder = GetReCaptchaTag("div", siteKey, callback);
            return GetHtmlContent(htmlHelper, tagBuilder);
        }

        public static IHtmlContent GoogleInvisibleReCaptcha(this IHtmlHelper htmlHelper, String text)
        {
            var configuration = htmlHelper.ViewContext.HttpContext.RequestServices.GetService<IConfiguration>();
            var siteKey = configuration.GetValue<String>("GoogleReCaptcha:ClientKey");
            var callback = "captchaSubmitForm";

            var tagBuilder = GetReCaptchaTag("button", siteKey, callback);
            tagBuilder.InnerHtml.Append(text);
            return GetHtmlContent(htmlHelper, tagBuilder);
        }

        private static TagBuilder GetReCaptchaTag(String tagName, String siteKey, String callback = null)
        {
            var tagBuilder = new TagBuilder(tagName);
            tagBuilder.Attributes.Add("class", "g-recaptcha btn btn-primary");
            tagBuilder.Attributes.Add("data-sitekey", siteKey);
            if (callback != null && !String.IsNullOrWhiteSpace(callback))
            {
                tagBuilder.Attributes.Add("data-callback", callback);
            }

            return tagBuilder;
        }

        private static IHtmlContent GetHtmlContent(IHtmlHelper htmlHelper, TagBuilder tagBuilder)
        {
            using (var writer = new StringWriter())
            {
                tagBuilder.WriteTo(writer, System.Text.Encodings.Web.HtmlEncoder.Default);
                var htmlOutput = writer.ToString();
                return htmlHelper.Raw(htmlOutput);
            }
        }
    }
}

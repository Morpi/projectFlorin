﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Api;
using Core.Api.Items;
using Core.Cache;
using Core.SignalR;
using Core.SignalR.Client;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebUI.Helpers;

namespace WebUI.Controllers
{
    [Authorize]
    public class PendingOrdersController : ExchangeControllerBase
    {
        private readonly ExchangeCache _cache;
        private readonly ExchangeContext _dbContext;
        private readonly ExchangeHelper _helper;
        private readonly MarketOrderClient _signalRClient;
        private readonly LogRepository _logger;
        private readonly TradeApi _tradeApi;

        public PendingOrdersController(ExchangeCache cache, ExchangeContext dbContext, ExchangeHelper helper, MarketOrderClient signalRClient, LogRepository logger, TradeApi tradeApi)
        {
            _cache = cache;
            _dbContext = dbContext;
            _helper = helper;
            _signalRClient = signalRClient;
            _logger = logger;
            _tradeApi = tradeApi;
        }

        [Route("pending-orders")]
        public IActionResult Index()
        {
            try
            {
                var pendingOrders = _dbContext.PendingOrders
                    .Where(po => po.UserId == UserId)
                    .OrderByDescending(po => po.CreatedAt)
                    .ToList();

                var markets = _cache.LimitedMarkets;
                var data = pendingOrders.Select(c =>
                {
                    var market = markets.Single(m => m.Code == c.MarketCode);
                    var result = new
                    {
                        id = c.Id,
                        marketCode = c.MarketCode,
                        dateAndTime = _helper.ToUnixTime(c.CreatedAt),
                        from = market.From,
                        to = market.To,
                        type = c.Type.ToString(),
                        price = c.Price,
                        quantity = c.Quantity,
                        filled = c.Filled
                    };
                    return result;
                });
                var model = JsonConvert.SerializeObject(new
                {
                    pendingOrders = data,
                    nrOfDecimals = 8
                });
                return View((object)model);
            }
            catch (Exception ex)
            {
                _logger.InsertError(LogCategory.PendingOrdersControllerIndex, ex);
                return View();
            }
        }

        [HttpPost]
        public ApiResult RemoveOrder(Guid id)
        {
            return _tradeApi.RemoveOrder(UserId,id);
        }
    }
}
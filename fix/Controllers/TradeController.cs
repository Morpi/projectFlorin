﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Storage;
using Core.Storage.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebUI.Helpers;
using Core.Cache;
using Core.Api.Items;

namespace WebUI.Controllers
{
    public class TradeController : ExchangeControllerBase
    {
        private readonly ExchangeContext _dbContext;
        private readonly LogRepository _logger;
        private readonly ExchangeHelper _helper;
        private readonly ExchangeCache _cache;
        private readonly TradeApi _tradeApi;

        public TradeController(ExchangeContext dbContext, LogRepository logger, ExchangeHelper helper, ExchangeCache cache, TradeApi tradeApi)
        {
            _dbContext = dbContext;
            _logger = logger;
            _helper = helper;
            _cache = cache;
            _tradeApi = tradeApi;
        }

        [HttpGet]
        [Route("/trade/{marketCode}")]
        public IActionResult Index(string marketCode)
        {
            if (marketCode == null)
            {
                throw new Exception("Market not found");
            }
            var market = _dbContext.Markets.SingleOrDefault(m => m.Code == marketCode.ToLower());
           
            if (market == null)
            {
                throw new Exception("Market not found");
            }
            var currencies = _dbContext.Currencies.Where(c => c.Code == market.From || c.Code == market.To);
            var fromCurrency = currencies.Single(c => c.Code == market.From);
            var toCurrency = currencies.Single(c => c.Code == market.To);
            var configurationSettings = _cache.ConfigurationSettings;

            var lastPrices = _dbContext.FilledOrders
                .Where(fo => fo.MarketCode == marketCode)
                .OrderByDescending(fo => fo.CreatedAt)
                .ThenByDescending(fo => fo.GropedOrder)
                .Take(2).ToList();

            var data = new
            {
                toTitle = toCurrency.Title,
                from = market.From,
                to = market.To,
                minTotal = fromCurrency.MinTrading,
                maxTotal = fromCurrency.MaxTrading,
                priceFromUsd = fromCurrency.PriceUsd,
                fee = configurationSettings.TRANSACTION_FEE,
                marketCode = marketCode,
                historySize = configurationSettings.HISTORY_SIZE,
                menu = new
                {
                    lastPrice = lastPrices.Any() ? new Nullable<decimal>(lastPrices.First().Price) : null,
                    lastPriceType = lastPrices.Any() ? lastPrices.First().Type.ToString() : null,
                    prevLastPrice = lastPrices.Count > 1 ? new Nullable<decimal>(lastPrices[1].Price) : null,
                    last24hPrice = market?.Last24hPrice,
                    _24hHigh = market?._24hHigh,
                    _24hLow = market?._24hLow,
                    _24hVolume = market?._24hVolume,
                }
            };

            ViewBag.JsonData = JsonConvert.SerializeObject(data);
            ViewBag.NrOfDecimals = market.NrOfDecimals;
            ViewBag.NrOfDecimalsToUSD = toCurrency.NrOfDecimalsUSD;
            return View();
        }

        [HttpPost]
        public IActionResult AddOrder(string marketCode, OrderType type, decimal price, decimal quantity)
        {
            var result = _tradeApi.AddOrder(UserId, marketCode, type, price, quantity);
            return new JsonResult(result);
        }
        
        [HttpPost]
        public IActionResult RemoveOrder(Guid id)
        {
            var result = _tradeApi.RemoveOrder(UserId, id);
            return new JsonResult(result);
        }

        [HttpGet]
        public IActionResult GetTradingViewData(string marketCode, int from, int to, string resolution)
        {
            var fromDateTime = _helper.FromUnixTime(from);
            var toDateTime = _helper.FromUnixTime(to);
            var intResolution = 0;
            string unitType = null;
            int unitValue;
            if (Int32.TryParse(resolution, out intResolution))
            {
                unitType = intResolution < 60 ? "Minute" : "Hour";
                unitValue = intResolution < 60 ? intResolution : intResolution / 60;
            }
            else
            {
                unitValue = 1;
                unitType = "Day";
            }
            var result = _tradeApi.GetTradingViewData(marketCode, fromDateTime, toDateTime, unitType, unitValue);
            return new JsonResult(result);
        }
    }
}
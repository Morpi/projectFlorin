﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Core.Storage;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;
using WebUI.Helpers;
using Microsoft.AspNetCore.Identity.UI.Services;
using Core.Wallets.Wallets;
using Core.Storage.Repositories;
using Core;
using Core.Wallets.Clients;
using Core.Wallets;
using Core.Cache;
using Core.Api.Items;
using Core.SignalR.Client;
using Core.SignalR.Server;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using Core.Security;
using Core.Email;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddDataProtection()
                         .UseCryptographicAlgorithms(new AuthenticatedEncryptorConfiguration()
                         {
                             EncryptionAlgorithm = EncryptionAlgorithm.AES_256_GCM,
                             ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
                         });

            services.AddTransient<LogRepository>();
            services.AddTransient<ExchangeHelper>();
            services.AddTransient<WalletHelper>();
            services.AddTransient<ExchangeCache>();
            services.AddTransient<TradeApi>();
            services.AddTransient<MarketOrderClient>();
            services.AddTransient<ExchangeConfig>();
            services.AddTransient<EncryptionService>();
            services.AddTransient<EmailTemplates>();

            /*
            services.AddSingleton<IHostingEnvironment>(new HostingEnvironment());
            var environment = new HostingEnvironment();
            services.AddSingleton<IConfiguration>(new ConfigurationBuilder()
               .SetBasePath(environment.ContentRootPath)
               .AddJsonFile($"appsettings.json")
               .Build());
               */
            services.AddTransient<IEmailSender, EmailSender>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ExchangeContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);
            /*
            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.User.RequireUniqueEmail = false;
            })
            */
            services.AddDefaultIdentity<IdentityUser>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ExchangeContext>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AddAreaPageRoute("Identity", "/account/login", "/account/login");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/register", "/account/register");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/forgotpassword", "/account/forgot-password");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/confirmemail", "/account/confirm-email");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/forgotpasswordconfirmation", "/account/forgot-password-confirmation");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/index", "/account/manage");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/changepassword", "/account/change-password");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/twoFactorAuthentication", "/account/two-factor-authentication");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/enableAuthenticator", "/account/enable-authenticator");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/registermessage", "/account/register-message");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/resetpassword", "/account/reset-password");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/ResetPasswordConfirmation", "/account/reset-password-confirmation");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/showrecoverycodes", "/account/show-recovery-codes");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/LoginWith2fa", "/account/login-with-2fa");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/manage/GenerateRecoveryCodes", "/account/generate-recovery-codes");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/LoginWithRecoveryCode", "/account/login-with-recovery-code");
                    options.Conventions.AddAreaPageRoute("Identity", "/account/Manage/Disable2fa", "/account/disable-2fa");
                    options.Conventions.AddAreaPageRoute("Identity", "/Account/Manage/ResetAuthenticator", "/account/reset-authenticator");
                });

            services.Configure<EmailSettings>(Configuration.GetSection("AppSettings"));

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                options.User.RequireUniqueEmail = false;
                options.SignIn.RequireConfirmedEmail = false;
            });

            services.AddSignalR()
              .AddMessagePackProtocol();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                /*
                var options = new RewriteOptions();
                options.AddRedirectToHttps();
                options.Rules.Add(new RedirectToWwwRule());
                app.UseRewriter(options);
                */
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<MarketOrderHub>("/marketOrder");
                routes.MapHub<TestHub>("/testSocket");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
